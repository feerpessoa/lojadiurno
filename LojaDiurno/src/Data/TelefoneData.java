package Data;






import business.Telefone;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TelefoneData {

    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List<Telefone> selectTelefone() {
        List<Telefone> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Telefone");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {

            e.printStackTrace();
            return new ArrayList<Telefone>();
        }
        return lista;
    }
    
    public List selectPorPessoa(int IdCliente)
    {
        List<Telefone> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Telefone tel join tel.tipotelefone tp where idPessoa = " + IdCliente);
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {

            e.printStackTrace();
            return new ArrayList<Telefone>();
        }
        return lista;
    }

    public String saveTelefone(Telefone telefone) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(telefone);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deleteTelefone(Telefone telefone) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(telefone);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Telefone getTelefone(int idTelefone) {
        List<Telefone> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Telefone where idTelefone = " + idTelefone);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
}
