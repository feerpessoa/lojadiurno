package Data;

import business.Enderecopessoa;
import business.Telefone;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EnderecoPessoaData {
    
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List select() {
        List<Enderecopessoa> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Enderecopessoa");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Enderecopessoa>();
        }
        return lista;
    }

    public String insertUpdate(Enderecopessoa enderecoPessoa) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(enderecoPessoa);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteEnderecoPessoa(Enderecopessoa enderecopessoa) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(enderecopessoa);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }
    
    public String saveEnderecoPessoa(Enderecopessoa enderecopessoa) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(enderecopessoa);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public Enderecopessoa getEnderecoPessoa(int idEndereco, int idPessoa) {
        List<Enderecopessoa> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Enderecopessoa where idEndereco = " + idEndereco + " and idPessoa = " + idPessoa);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public List selectPorPessoa(int idPessoa) {
        List<Enderecopessoa> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Enderecopessoa as ep join ep.endereco as e join e.bairro as b join b.cidade as c where idpessoa ="+idPessoa);
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Enderecopessoa>();
        }
        return lista;
    }
    
}
