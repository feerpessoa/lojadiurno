package Data;

import business.Pessoafisica;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PessoaFisicaData {

    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectPessoaFisica() {
        List<Pessoafisica> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Pessoafisica");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
           
            e.printStackTrace();
            return new ArrayList<Pessoafisica>();
        }
        return lista;
    }

    public String savePessoaFisica(Pessoafisica pessoaFisica) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(pessoaFisica);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deletePessoafisica(Pessoafisica pessoaFisica) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(pessoaFisica);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;

    }
    
    public Pessoafisica getPessoafisica(int idPessoafisica) {
        List<Pessoafisica> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Pessoafisica where idPessoaFisica = " + idPessoafisica);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            
            e.printStackTrace();
            return null;
        }
        
    }
}