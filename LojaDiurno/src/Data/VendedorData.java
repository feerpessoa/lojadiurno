package Data;

import business.Vendedor;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class VendedorData {

    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectVendedor() {
        List<Vendedor> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Vendedor");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
           
            e.printStackTrace();
            return new ArrayList<Vendedor>();
        }
        return lista;
    }

    public String saveVendedor(Vendedor vendedor) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(vendedor);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deleteVendedor(Vendedor vendedor) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(vendedor);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;

    }
    
    public Vendedor getVendedor(int idVendedor) {
        List<Vendedor> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Vendedor where idVendedor = " + idVendedor);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            
            e.printStackTrace();
            return null;
        }
        
    }
}
