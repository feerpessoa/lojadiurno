package Data;

import business.Tipotelefone;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TipoTelefoneData {
    private Session sessao;
    private Transaction transacao;
    private Query query;

     public List<Tipotelefone> selectTipotelefone() {
        List<Tipotelefone> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Tipotelefone");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {

            e.printStackTrace();
            return new ArrayList<Tipotelefone>();
        }
        return lista;
    }

    public String saveTipotelefone(Tipotelefone telefone) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(telefone);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deleteTipotelefone(Tipotelefone telefone) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(telefone);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Tipotelefone getTipotelefone(int idTipotelefone) {
        List<Tipotelefone> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Tipotelefone where idTipotelefone = " + idTipotelefone);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
}