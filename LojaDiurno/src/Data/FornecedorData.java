package Data;

import business.Fornecedor;
import business.Pessoa;
import business.Pessoafisica;
import business.Pessoajuridica;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class FornecedorData {

    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectFornecedor() {
        List<Fornecedor> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Fornecedor");
            lista = query.list();
            sessao.close();
            
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<Fornecedor>();
        }
        return lista;
    }

    public String saveFornecedor(Fornecedor fornecedor) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            if (fornecedor.getIdFornecedor() == 0) {
                Pessoa p = fornecedor.getPessoajuridica().getPessoa();
                sessao.save(p);
                
                Pessoajuridica pj = fornecedor.getPessoajuridica();
                pj.setPessoa(p);
                sessao.save(pj);
                fornecedor.setPessoajuridica(pj);
                sessao.saveOrUpdate(fornecedor);
            } else {
                //Pessoa p = fornecedor.getPessoajuridica().getPessoa();
                //sessao.update(p);
                //Pessoajuridica pj = fornecedor.getPessoajuridica();
                //pj.setPessoa(p);
                //sessao.update(pj);
                //fornecedor.setPessoajuridica(pj);
                //sessao.update(fornecedor.getPessoajuridica().getPessoa());
                //fornecedor.getPessoajuridica().getPessoa().setPessoajuridica(fornecedor.getPessoajuridica());
                sessao.update(fornecedor);
            }
            //sessao.saveOrUpdate(fornecedor.getPessoajuridica());
            //sessao.saveOrUpdate(fornecedor.getPessoajuridica().getPessoa());
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public String deleteFornecedor(Fornecedor fornecedor) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(fornecedor);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;
    }

    public Fornecedor getFornecedor(int idFornecedor) {
        List<Fornecedor> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Fornecedor where idFornecedor = " + idFornecedor);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
}
