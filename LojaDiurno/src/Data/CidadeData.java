package Data;

import business.Cidade;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CidadeData {
    
    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectCidade() {
        List<Cidade> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Cidade");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
           
            e.printStackTrace();
            return new ArrayList<Cidade>();
        }
        return lista;
    }

    public String saveCidade(Cidade cidade) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(cidade);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deleteCidade(Cidade cidade) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(cidade);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;

    }
    
    public Cidade getCidade(int idCidade) {
        List<Cidade> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Cidade where idCidade = " + idCidade);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            
            e.printStackTrace();
            return null;
        }
        
    }
    
}
