package Data;

import business.Cliente;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ClienteData {

    private Session sessao;
    private Transaction transacao;
    private Query query;

    public List selectCliente() {
        List<Cliente> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Cliente");
            lista = query.list();
            sessao.close();
        } catch (HibernateException e) {
           
            e.printStackTrace();
            return new ArrayList<Cliente>();
        }
        return lista;
    }
    
    public String salvarClientePessoaFisica(Cliente cliente)
    {
        String erro = null;
        
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(cliente);
            
            cliente.getPessoa().getPessoafisica().setIdPessoaFisica(cliente.getIdCliente());
            cliente.getPessoa().getPessoafisica().setPessoa(cliente.getPessoa());
            
            sessao.save(cliente.getPessoa().getPessoafisica());
            transacao.commit();
            sessao.close();
        }
        
        catch (HibernateException e)
        {
            erro = e.getMessage();
            e.printStackTrace();
        }
        
        return erro;
    }
    
    public String salvarClientePessoaJuridica(Cliente cliente)
    {
        String erro = null;
        
        try
        {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(cliente);
            
            cliente.getPessoa().getPessoajuridica().setIdPessoaJuridica(cliente.getIdCliente());
            cliente.getPessoa().getPessoajuridica().setPessoa(cliente.getPessoa());
            
            sessao.save(cliente.getPessoa().getPessoajuridica());
            transacao.commit();
            sessao.close();
        }
        
        catch(HibernateException e)
        {
            erro = e.getMessage();
            e.printStackTrace();
        }
        
        return erro;
    }
    
    public String saveCliente(Cliente cliente) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.saveOrUpdate(cliente);
            transacao.commit();
            sessao.close();

        } catch (HibernateException e) {
            erro = e.getMessage();

            e.printStackTrace();
        }
        return erro;
    }

    public String deleteCliente(Cliente cliente) {
        String erro = null;
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            sessao.delete(cliente);
            transacao.commit();
            sessao.close();
            
        } catch (HibernateException e) {
            erro = e.getMessage();
            e.printStackTrace();
        }
        return erro;

    }
    
    public Cliente getCliente(int idCliente)
    {
        List<Cliente> lista = new ArrayList();
        try {
            this.sessao = Sessao.getSessao();
            transacao = sessao.beginTransaction();
            query = sessao.createQuery("from Cliente where idCliente = " + idCliente);
            lista = query.list();
            sessao.close();
            return lista.get(0);
        } catch (HibernateException e) {
            
            e.printStackTrace();
            return null;
        }    
    }
    
    
}
