/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Data.BairroData;
import Data.CidadeData;
import Data.EnderecoData;
import Data.EnderecoPessoaData;
import Data.FornecedorData;
import Data.TelefoneData;
import Data.TipoTelefoneData;
import business.Bairro;
import business.Cidade;
import business.Endereco;
import business.Enderecopessoa;
import business.EnderecopessoaId;
import javax.swing.JOptionPane;
import business.Fornecedor;
import business.Pessoa;
import business.Pessoajuridica;
import business.Telefone;
import business.Tipotelefone;
import com.sun.org.apache.xpath.internal.operations.Number;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


public class FormCadastroFornecedor extends javax.swing.JFrame {
    
    private int idFornecedor;
    private int idEndereco;
    private Integer idTelefone = null;
    private Fornecedor fornecedor;
    private Enderecopessoa enderecoPessoa;
    private Endereco endereco;
    private FornecedorData fornecedorData;
    private EnderecoData enderecoData;
    private EnderecoPessoaData enderecopessoaData;
    private BairroData bairroData;
    private CidadeData cidadeData;
    private TelefoneData telefoneData;
    private TipoTelefoneData tipoTelefoneData;
    private Tipotelefone tipotelefone;
    private Telefone telefone;

    /**
     * Creates new form FormCadastroFornecedor
     */
    //public FormCadastroFornecedor(Fornecedor f) {
    public FormCadastroFornecedor() {
        
        initComponents();
        tabPrincipal.setEnabledAt(1,false);
        tabPrincipal.setEnabledAt(2,false);
                
        fornecedorData = new FornecedorData();
        enderecoData = new EnderecoData();
        enderecopessoaData = new EnderecoPessoaData();
        telefoneData = new TelefoneData();
        bairroData = new BairroData();
        cidadeData = new CidadeData();
        
        atualizarTabela();
        
        TipoTelefoneData ttd = new TipoTelefoneData();
        cbxTipoTelefone.removeAllItems();
        for (Tipotelefone t : ttd.selectTipotelefone()){
            cbxTipoTelefone.addItem(t.getDescricao());
        }
//        Telefone t = new Telefone();
//        t.setTipotelefone((Tipotelefone) cbxTipoTelefone.getSelectedItem());
//        //t.setPessoa(null);
//        //idFornecedor = 0;
    }
    
    
     private void atribuirFornecedor(Fornecedor f)
    {
        idFornecedor = f.getIdFornecedor();
        txtNome.setText(f.getPessoajuridica().getPessoa().getNome());
        txtEmailFornecedor.setText(f.getPessoajuridica().getPessoa().getEmail());
        txtCNPJ.setText(f.getPessoajuridica().getCnpj());
        txtRazaoSocial.setText(f.getPessoajuridica().getRazaoSocial());
        txtInscricaoEstadual.setText(f.getPessoajuridica().getIncricaoEstadual());
        txtSiteFornecedor.setText(f.getSite());
        txtObservacao.setText(f.getPessoajuridica().getPessoa().getObservacao());
        
    }
     
     private void obterFornecedor(){
         fornecedor = new Fornecedor();
         
         fornecedor.setIdFornecedor(idFornecedor);
         fornecedor.setSite(txtSiteFornecedor.getText());
         
         fornecedor.setPessoajuridica(new Pessoajuridica());
         
         fornecedor.getPessoajuridica().setIdPessoaJuridica(idFornecedor);
         
         fornecedor.getPessoajuridica().setCnpj(txtCNPJ.getText());
         fornecedor.getPessoajuridica().setFornecedor(fornecedor);
         fornecedor.getPessoajuridica().setIncricaoEstadual(txtInscricaoEstadual.getText());
         fornecedor.getPessoajuridica().setRazaoSocial(txtRazaoSocial.getText());
         fornecedor.getPessoajuridica().setPessoa(new Pessoa());
         fornecedor.getPessoajuridica().getPessoa().setIdPessoa(idFornecedor);
         fornecedor.getPessoajuridica().getPessoa().setNome(txtNome.getText());
         fornecedor.getPessoajuridica().getPessoa().setEmail(txtEmailFornecedor.getText());
         fornecedor.getPessoajuridica().getPessoa().setObservacao(txtObservacao.getText());
         
      }
     private void obterEndereco(){
         
         enderecoPessoa = new Enderecopessoa();
         enderecoPessoa.setId(new EnderecopessoaId(endereco.getIdEndereco(), fornecedor.getIdFornecedor()));
         enderecoPessoa.setPessoa(fornecedor.getPessoajuridica().getPessoa());
         enderecoPessoa.setEndereco(endereco);
         enderecoPessoa.setComplemeto(txtComplemento.getText());
         enderecoPessoa.setNumero(Short.parseShort(txtNumero.getText()));
         enderecoPessoa.setObservacao(txtObservacao.getText());
                  
     }
     private void atribuirEndereco(Enderecopessoa e){
         idEndereco = e.getId().getIdEndereco();
         txtComplemento.setText(e.getComplemeto());
         txtNumero.setText(String.valueOf(e.getNumero()));
         txtObservacao.setText(e.getObservacao());
         txtLogradouro.setText(e.getEndereco().getEnderecoCompleto()); //se for getLogradouro() ele pega só "Rua" ao invés de "Rua Tal..."
         txtCEP.setText(e.getEndereco().getCep());
         txtCidade.setText(e.getEndereco().getBairro().getCidade().getCidade());
         txtBairro.setText(e.getEndereco().getBairro().getBairro());
         cbxUF.setSelectedItem(e.getEndereco().getBairro().getCidade().getEstado().getUf());
     }
     
    
    private boolean validar()
    {
        if(txtNome.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Nome não informado!", "Nome não foi Informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(txtEmailFornecedor.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Email não informado!", "Email não informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        if(txtCNPJ.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "CNPJ não informado!", "CNPJ não informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(txtRazaoSocial.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Razao Social não informada!", "Razao Social não informada", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(txtInscricaoEstadual.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "Inscricao Estadual não informada!", "Inscricao Estadual não informada", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
    private boolean validarTelefone(){
        if(tabPrincipal.isEnabledAt(1) == true)
        {
            if(txtNumeroTelefone.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O numero telefônico não foi informado.", "Numero teleônico não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(txtOperadora.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A operadora do telefone não foi informado.", "Operadora não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
        }
     return true;
    }
    
    private Telefone obterTelefone()
    {
        Telefone telefone= new Telefone();
        //telefone.setIdTelefone(new TelefoneId(telefone.getIdTelefone(), fornecedor.getIdFornecedor()));
        TipoTelefoneData telefoneData = new TipoTelefoneData();
        List<Tipotelefone> t = telefoneData.selectTipotelefone();
        Tipotelefone tf = new Tipotelefone();
        for(Tipotelefone tt : t)
        {
            if(tt.getDescricao().equals(cbxTipoTelefone.getSelectedItem().toString()))
            {
                tf = tt;
            }
        }
        
        
        fornecedor.getPessoajuridica().setIdPessoaJuridica(fornecedor.getIdFornecedor());
        
        telefone.setPessoa(fornecedor.getPessoajuridica().getPessoa());
         telefone.setNumeroTelefone(txtNumeroTelefone.getText());
        telefone.setOperadora(txtOperadora.getText());
        //telefone.setTipotelefone((Tipotelefone)cbxTipoTelefone.getSelectedItem());
        telefone.setTipotelefone(tf);
        
        return telefone;
        
    }
    
    private void salvarTelefone(Telefone telefone)
    {
        telefoneData.saveTelefone(telefone);
    }
    
    public void atribuirTelefone(Telefone t)
    {
        txtNumeroTelefone.setText(t.getNumeroTelefone());
        txtOperadora.setText(String.valueOf(t.getOperadora()));
        cbxTipoTelefone.setSelectedItem(t);
    }
    
    public void limparTelefone()
    {
        txtNumeroTelefone.setText("");
        txtOperadora.setText("");
        cbxTipoTelefone.setSelectedIndex(0);
    }
    
    private boolean validarEndereco(){
        
        if(tabPrincipal.isEnabledAt(2) == true)
        {
            txtLogradouro.setEnabled(false);
            txtBairro.setEnabled(false);
            txtCidade.setEnabled(false);
            cbxUF.setEnabled(false);
            if(txtCEP.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "CEP deve ser informado.", "CEP não foi informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtNumero.getText().equals("") )
            {
                JOptionPane.showMessageDialog(txtComplemento, "Numero do endereço deve ser informado.", "Numero do Endereço não infomado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtLogradouro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O logradouro deve ser informado.", "Logradouro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtBairro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O Bairro deve ser informado.", "Bairro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtCidade.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A cidade deve ser informado.", "Cidade não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(cbxUF.getSelectedIndex() == 0)
            {
                JOptionPane.showMessageDialog(txtComplemento, "A unidade federativa ou sigla do estado deve ser informado.", "Unidade Federativa não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
        }
        
        return true;
    }
    
    private void limparEndereco(){
         txtComplemento.setText("");
         txtNumero.setText("");
         txtObservacao.setText("");
         txtLogradouro.setText("");
         txtCEP.setText("");
         txtCidade.setText("");
         txtBairro.setText("");
         cbxUF.setSelectedItem(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabPrincipal = new javax.swing.JTabbedPane();
        tabFornecedor = new javax.swing.JPanel();
        txtCNPJ = new javax.swing.JTextField();
        lblEmailFornecedor = new javax.swing.JLabel();
        txtEmailFornecedor = new javax.swing.JTextField();
        lblRazaoSocial = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        btnSalvarContinuar1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblFornecedor = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        label13 = new java.awt.Label();
        lblLogo = new javax.swing.JLabel();
        lblTituloFornecedor = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblCNPJFornecedor = new javax.swing.JLabel();
        lblObservacao = new javax.swing.JLabel();
        label14 = new java.awt.Label();
        lblSiteFornecedor = new javax.swing.JLabel();
        txtInscricaoEstadual = new javax.swing.JTextField();
        txtRazaoSocial = new javax.swing.JTextField();
        lblInscriacaoEstadual = new javax.swing.JLabel();
        txtSiteFornecedor = new javax.swing.JTextField();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtObservacao = new javax.swing.JTextArea();
        txtNome = new javax.swing.JTextField();
        label16 = new java.awt.Label();
        label17 = new java.awt.Label();
        tabTelefone = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        btnEditarTelefone = new javax.swing.JButton();
        lblNumeroTelefone = new javax.swing.JLabel();
        btnSalvarContinuar2 = new javax.swing.JButton();
        jLabel32 = new javax.swing.JLabel();
        lblOperadora = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        txtOperadora = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        cbxTipoTelefone = new javax.swing.JComboBox();
        label6 = new java.awt.Label();
        txtNumeroTelefone = new javax.swing.JFormattedTextField();
        btnExcluirTelefone = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblFornecedorTelefone = new javax.swing.JTable();
        btnNovoEndereco1 = new javax.swing.JButton();
        btnNovoTelefone = new javax.swing.JButton();
        tabEndereco = new javax.swing.JPanel();
        lblCidade = new javax.swing.JLabel();
        txtComplemento = new javax.swing.JTextField();
        txtLogradouro = new javax.swing.JTextField();
        txtBairro = new javax.swing.JTextField();
        txtCidade = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        label7 = new java.awt.Label();
        btnEditarEndereco = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btnSalvarFinalizar = new javax.swing.JButton();
        lblTitulo1 = new javax.swing.JLabel();
        label8 = new java.awt.Label();
        lblCEP = new javax.swing.JLabel();
        label9 = new java.awt.Label();
        label10 = new java.awt.Label();
        txtNumero = new javax.swing.JTextField();
        lblNumero = new javax.swing.JLabel();
        label11 = new java.awt.Label();
        label12 = new java.awt.Label();
        lblComplemento = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        lblObservacaoFornecedorEnd = new javax.swing.JLabel();
        lblBairro = new javax.swing.JLabel();
        lblLogradouro = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        cbxUF = new javax.swing.JComboBox();
        txtCEP = new javax.swing.JFormattedTextField();
        label15 = new java.awt.Label();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblFornecedorEndereco = new javax.swing.JTable();
        btnNovoEndereco = new javax.swing.JButton();
        btnExcluirEndereco = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabPrincipal.setName("tabPrincipal"); // NOI18N

        tabFornecedor.setName("tabFornecedor"); // NOI18N
        tabFornecedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtCNPJ.setName("txtCNPJ"); // NOI18N
        txtCNPJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCNPJActionPerformed(evt);
            }
        });
        tabFornecedor.add(txtCNPJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 248, 213, -1));

        lblEmailFornecedor.setText("Email:");
        lblEmailFornecedor.setName("lblEmail"); // NOI18N
        tabFornecedor.add(lblEmailFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 205, -1, -1));

        txtEmailFornecedor.setName("txtCNPJ"); // NOI18N
        txtEmailFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailFornecedorActionPerformed(evt);
            }
        });
        tabFornecedor.add(txtEmailFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 202, 213, -1));

        lblRazaoSocial.setText("Razão Social");
        tabFornecedor.add(lblRazaoSocial, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 297, -1, -1));

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setName("btnCancelar"); // NOI18N
        tabFornecedor.add(btnCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 604, -1, -1));

        btnSalvarContinuar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvarContinuar1.setText("Salvar e Continuar");
        btnSalvarContinuar1.setName("btnSalvar"); // NOI18N
        btnSalvarContinuar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarContinuar1ActionPerformed(evt);
            }
        });
        tabFornecedor.add(btnSalvarContinuar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(784, 604, -1, -1));

        tblFornecedor.setAutoCreateColumnsFromModel(false);
        tblFornecedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblFornecedor.setName("tabelaCadastroFornecedor"); // NOI18N
        tblFornecedor.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(tblFornecedor);

        tabFornecedor.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 371, 545, 214));

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setName("btnEditar"); // NOI18N
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        tabFornecedor.add(btnEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(678, 604, -1, -1));

        label13.setForeground(new java.awt.Color(204, 0, 0));
        label13.setText("*");
        tabFornecedor.add(label13, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 341, -1, -1));

        lblLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N
        tabFornecedor.add(lblLogo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        lblTituloFornecedor.setFont(new java.awt.Font("Arial Unicode MS", 0, 24)); // NOI18N
        lblTituloFornecedor.setForeground(new java.awt.Color(102, 102, 102));
        lblTituloFornecedor.setText("Cadastro de Fornecedor");
        tabFornecedor.add(lblTituloFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(228, 43, -1, 62));

        lblNome.setFont(new java.awt.Font("Arial Unicode MS", 0, 12)); // NOI18N
        lblNome.setText("Nome:");
        lblNome.setName("lblNome"); // NOI18N
        tabFornecedor.add(lblNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 156, -1, -1));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Add User-100.png"))); // NOI18N
        tabFornecedor.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 220, -1, -1));

        lblCNPJFornecedor.setFont(new java.awt.Font("Arial Unicode MS", 0, 11)); // NOI18N
        lblCNPJFornecedor.setText("CNPJ:");
        lblCNPJFornecedor.setName("lblCNPJFornecedor"); // NOI18N
        tabFornecedor.add(lblCNPJFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 250, 72, -1));

        lblObservacao.setFont(new java.awt.Font("Arial Unicode MS", 0, 11)); // NOI18N
        lblObservacao.setText("Observação:");
        lblObservacao.setName("lblObservacao"); // NOI18N
        tabFornecedor.add(lblObservacao, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 447, 96, -1));

        label14.setForeground(new java.awt.Color(204, 0, 0));
        label14.setText("*");
        tabFornecedor.add(label14, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 248, -1, -1));

        lblSiteFornecedor.setFont(new java.awt.Font("Arial Unicode MS", 0, 11)); // NOI18N
        lblSiteFornecedor.setText("Site:");
        lblSiteFornecedor.setName("lblSite"); // NOI18N
        tabFornecedor.add(lblSiteFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 395, 96, -1));

        txtInscricaoEstadual.setName("txtSite"); // NOI18N
        tabFornecedor.add(txtInscricaoEstadual, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 341, 213, -1));

        txtRazaoSocial.setName("txtCNPJ"); // NOI18N
        txtRazaoSocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRazaoSocialActionPerformed(evt);
            }
        });
        tabFornecedor.add(txtRazaoSocial, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 294, 213, -1));

        lblInscriacaoEstadual.setText("Inscrição Estadual");
        tabFornecedor.add(lblInscriacaoEstadual, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 344, -1, -1));

        txtSiteFornecedor.setName("txtCNPJ"); // NOI18N
        txtSiteFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSiteFornecedorActionPerformed(evt);
            }
        });
        tabFornecedor.add(txtSiteFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 393, 213, -1));

        label1.setForeground(new java.awt.Color(204, 0, 0));
        label1.setText("*");
        tabFornecedor.add(label1, new org.netbeans.lib.awtextra.AbsoluteConstraints(551, 155, -1, -1));

        label2.setForeground(new java.awt.Color(204, 0, 0));
        label2.setText("*");
        tabFornecedor.add(label2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 200, -1, -1));

        txtObservacao.setColumns(20);
        txtObservacao.setRows(5);
        txtObservacao.setName("txtObservacao"); // NOI18N
        jScrollPane5.setViewportView(txtObservacao);

        tabFornecedor.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(138, 447, 213, 138));

        txtNome.setName("txtNome"); // NOI18N
        tabFornecedor.add(txtNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 155, 401, -1));

        label16.setForeground(new java.awt.Color(204, 0, 0));
        label16.setText("*");
        tabFornecedor.add(label16, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 294, -1, -1));

        label17.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        label17.setForeground(new java.awt.Color(204, 0, 0));
        label17.setText("* Campos Obrigatórios");
        tabFornecedor.add(label17, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 603, -1, -1));

        tabPrincipal.addTab("Cadastro de Fornecedor", tabFornecedor);

        tabTelefone.setName("tabTelefone"); // NOI18N

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N

        lblTitulo.setFont(new java.awt.Font("Arial Unicode MS", 0, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo.setText("Cadastro de Fornecedor - Telefone");

        btnEditarTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditarTelefone.setText("Editar");
        btnEditarTelefone.setName("btnEditar"); // NOI18N
        btnEditarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarTelefoneActionPerformed(evt);
            }
        });

        lblNumeroTelefone.setText("Número do telefone:");

        btnSalvarContinuar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvarContinuar2.setLabel("Salvar e Continuar");
        btnSalvarContinuar2.setName("btnSalvar"); // NOI18N
        btnSalvarContinuar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarContinuar2ActionPerformed(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 0, 51));
        jLabel32.setText("*");

        lblOperadora.setText("Operadora:");

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 0, 51));
        jLabel33.setText("*");

        txtOperadora.setName("txtOperadora"); // NOI18N

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 0, 51));
        jLabel34.setText("*");

        lblTipo.setText("Tipo:");

        cbxTipoTelefone.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione...", "Residencial", "Comercial", "Celular", "Recado" }));
        cbxTipoTelefone.setName("comboItem"); // NOI18N
        cbxTipoTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxTipoTelefoneActionPerformed(evt);
            }
        });

        label6.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        label6.setForeground(new java.awt.Color(204, 0, 0));
        label6.setText("* Campos Obrigatórios");

        try {
            txtNumeroTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtNumeroTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroTelefoneActionPerformed(evt);
            }
        });

        btnExcluirTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnExcluirTelefone.setText("Excluir");
        btnExcluirTelefone.setName("btnEditar"); // NOI18N
        btnExcluirTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirTelefoneActionPerformed(evt);
            }
        });

        tblFornecedorTelefone.setAutoCreateColumnsFromModel(false);
        tblFornecedorTelefone.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblFornecedorTelefone.setName("tabelaCadastroFornecedor"); // NOI18N
        jScrollPane7.setViewportView(tblFornecedorTelefone);

        btnNovoEndereco1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnNovoEndereco1.setText("Novo");
        btnNovoEndereco1.setName("btnEditar"); // NOI18N

        btnNovoTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnNovoTelefone.setText("Novo");
        btnNovoTelefone.setName("btnEditar"); // NOI18N
        btnNovoTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoTelefoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tabTelefoneLayout = new javax.swing.GroupLayout(tabTelefone);
        tabTelefone.setLayout(tabTelefoneLayout);
        tabTelefoneLayout.setHorizontalGroup(
            tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabTelefoneLayout.createSequentialGroup()
                .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(lblTitulo))
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(tabTelefoneLayout.createSequentialGroup()
                                .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnNovoTelefone)
                                .addGap(18, 18, 18)
                                .addComponent(btnExcluirTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnEditarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(btnSalvarContinuar2))
                            .addGroup(tabTelefoneLayout.createSequentialGroup()
                                .addComponent(lblNumeroTelefone)
                                .addGap(4, 4, 4)
                                .addComponent(txtNumeroTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(tabTelefoneLayout.createSequentialGroup()
                                .addComponent(lblOperadora)
                                .addGap(47, 47, 47)
                                .addComponent(txtOperadora, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(tabTelefoneLayout.createSequentialGroup()
                                .addComponent(lblTipo)
                                .addGap(79, 79, 79)
                                .addComponent(cbxTipoTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 891, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(52, 52, 52))
            .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(tabTelefoneLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(btnNovoEndereco1)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        tabTelefoneLayout.setVerticalGroup(
            tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabTelefoneLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(41, 41, 41)
                .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblNumeroTelefone))
                    .addComponent(txtNumeroTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel32)))
                .addGap(18, 18, 18)
                .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblOperadora))
                    .addComponent(txtOperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel33)))
                .addGap(18, 18, 18)
                .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblTipo))
                    .addComponent(cbxTipoTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(tabTelefoneLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel34)))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalvarContinuar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditarTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnExcluirTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNovoTelefone)))
                .addContainerGap())
            .addGroup(tabTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(tabTelefoneLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(btnNovoEndereco1)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        tabPrincipal.addTab("Cadastro de Fornecedor - Telefone", tabTelefone);

        tabEndereco.setName("tabEndereco"); // NOI18N

        lblCidade.setText("Cidade:");

        txtComplemento.setName("txtComplemento"); // NOI18N

        txtLogradouro.setEnabled(false);
        txtLogradouro.setName("txtLogadouro"); // NOI18N

        txtBairro.setEnabled(false);
        txtBairro.setName("txtBairro"); // NOI18N
        txtBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBairroActionPerformed(evt);
            }
        });

        txtCidade.setEnabled(false);
        txtCidade.setName("txtCidade"); // NOI18N

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setName("txtObservacao"); // NOI18N
        jScrollPane2.setViewportView(jTextArea1);

        label7.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        label7.setForeground(new java.awt.Color(204, 0, 0));
        label7.setText("* Campos Obrigatórios");

        btnEditarEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnEditarEndereco.setText("Editar");
        btnEditarEndereco.setName("btnEditar"); // NOI18N
        btnEditarEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarEnderecoActionPerformed(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N

        btnSalvarFinalizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Checkmark-26.png"))); // NOI18N
        btnSalvarFinalizar.setActionCommand("Salvar e Finalizar");
        btnSalvarFinalizar.setLabel("Salvar e Finalizar");
        btnSalvarFinalizar.setName("btnSalvar"); // NOI18N
        btnSalvarFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarFinalizarActionPerformed(evt);
            }
        });

        lblTitulo1.setFont(new java.awt.Font("Arial Unicode MS", 0, 24)); // NOI18N
        lblTitulo1.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo1.setText("Cadastro de Fornecedor - Endereço");

        label8.setForeground(new java.awt.Color(204, 0, 0));
        label8.setText("*");

        lblCEP.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblCEP.setText("CEP:");
        lblCEP.setName("lblCEP"); // NOI18N

        label9.setForeground(new java.awt.Color(204, 0, 0));
        label9.setText("*");

        label10.setForeground(new java.awt.Color(204, 0, 0));
        label10.setText("*");

        txtNumero.setName("txtNumero"); // NOI18N
        txtNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroActionPerformed(evt);
            }
        });

        lblNumero.setText("Número:");
        lblNumero.setName("lblNumero"); // NOI18N

        label11.setForeground(new java.awt.Color(204, 0, 0));
        label11.setText("*");

        label12.setForeground(new java.awt.Color(204, 0, 0));
        label12.setText("*");

        lblComplemento.setText("Complemento:");

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/search.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        lblObservacaoFornecedorEnd.setText("Observação:");
        lblObservacaoFornecedorEnd.setName("lblObservacao"); // NOI18N

        lblBairro.setText("Bairro:");
        lblBairro.setName("lblBairro"); // NOI18N

        lblLogradouro.setText("Logradouro:");

        jLabel26.setText("UF:");

        cbxUF.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione..", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PR", "PB", "PA", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SE", "SP", "TO" }));
        cbxUF.setEnabled(false);
        cbxUF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxUFActionPerformed(evt);
            }
        });

        try {
            txtCEP.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCEP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCEPActionPerformed(evt);
            }
        });

        label15.setForeground(new java.awt.Color(204, 0, 0));
        label15.setText("*");

        tblFornecedorEndereco.setAutoCreateColumnsFromModel(false);
        tblFornecedorEndereco.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblFornecedorEndereco.setName("tabelaCadastroFornecedor"); // NOI18N
        jScrollPane6.setViewportView(tblFornecedorEndereco);

        btnNovoEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Edit.png"))); // NOI18N
        btnNovoEndereco.setText("Novo");
        btnNovoEndereco.setName("btnEditar"); // NOI18N
        btnNovoEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoEnderecoActionPerformed(evt);
            }
        });

        btnExcluirEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnExcluirEndereco.setText("Excluir");
        btnExcluirEndereco.setName("btnEditar"); // NOI18N
        btnExcluirEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirEnderecoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tabEnderecoLayout = new javax.swing.GroupLayout(tabEndereco);
        tabEndereco.setLayout(tabEnderecoLayout);
        tabEnderecoLayout.setHorizontalGroup(
            tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabEnderecoLayout.createSequentialGroup()
                .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(lblTitulo1))
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(lblCEP)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(label8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(btnBuscar)
                        .addGap(65, 65, 65)
                        .addComponent(lblLogradouro)
                        .addGap(11, 11, 11)
                        .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(label15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(lblObservacaoFornecedorEnd))
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(tabEnderecoLayout.createSequentialGroup()
                                .addComponent(lblBairro)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(tabEnderecoLayout.createSequentialGroup()
                                .addComponent(lblNumero)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(tabEnderecoLayout.createSequentialGroup()
                                .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(129, 129, 129)
                                .addComponent(lblComplemento)
                                .addGap(11, 11, 11)
                                .addComponent(txtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(tabEnderecoLayout.createSequentialGroup()
                                .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(181, 181, 181)
                                .addComponent(lblCidade)
                                .addGap(13, 13, 13)
                                .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(jLabel26)
                                .addGap(3, 3, 3)
                                .addComponent(cbxUF, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13)
                                .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(tabEnderecoLayout.createSequentialGroup()
                                .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnNovoEndereco)
                                .addGap(18, 18, 18)
                                .addComponent(btnExcluirEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnEditarEndereco)
                                .addGap(18, 18, 18)
                                .addComponent(btnSalvarFinalizar))
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(46, 46, 46))
        );
        tabEnderecoLayout.setVerticalGroup(
            tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabEnderecoLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(lblTitulo1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(45, 45, 45)
                .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCEP))
                    .addComponent(label8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addGroup(tabEnderecoLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblLogradouro))
                    .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblNumero))
                    .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblComplemento)
                    .addComponent(txtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblBairro))
                    .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCidade)
                    .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(cbxUF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblObservacaoFornecedorEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvarFinalizar)
                        .addComponent(btnEditarEndereco)
                        .addComponent(btnNovoEndereco)
                        .addComponent(btnExcluirEndereco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45))
        );

        txtCEP.getAccessibleContext().setAccessibleName("");
        txtCEP.getAccessibleContext().setAccessibleDescription("");

        tabPrincipal.addTab("Cadastro de Fornecedor - Endereço", tabEndereco);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 1001, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 704, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbxTipoTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxTipoTelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxTipoTelefoneActionPerformed

    private void txtBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBairroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBairroActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        endereco = enderecoData.getEnderecoCep(txtCEP.getText());
        txtLogradouro.setText(endereco.getEnderecoCompleto());
        txtBairro.setText(endereco.getBairro().getBairro());
        txtCidade.setText(endereco.getBairro().getCidade().getCidade());
        //txtNumero.setText(1+"");
        cbxUF.addItem(endereco.getBairro().getCidade().getEstado().getUf());
        cbxUF.setSelectedItem(endereco.getBairro().getCidade().getEstado().getUf());
       // FormBuscarEndereco buscar = new FormBuscarEndereco();
        //buscar.setVisible(true);
        
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtCNPJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCNPJActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCNPJActionPerformed

    private void txtEmailFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailFornecedorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailFornecedorActionPerformed

    private void txtRazaoSocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRazaoSocialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRazaoSocialActionPerformed

    private void txtSiteFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSiteFornecedorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSiteFornecedorActionPerformed

    private void btnSalvarContinuar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarContinuar1ActionPerformed
        if (!validar()) {
            
        } else {
             obterFornecedor();
            String erro = fornecedorData.saveFornecedor(fornecedor);
            if (erro != null && !erro.isEmpty()) {
                JOptionPane.showMessageDialog(null, erro);
                return;
            }
    
            tabPrincipal.setEnabledAt(1,true);
            tabPrincipal.setSelectedIndex(1);
            atualizarTabelaTelefone();
        }
                        
    }//GEN-LAST:event_btnSalvarContinuar1ActionPerformed

    private void btnSalvarFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarFinalizarActionPerformed
         if (validarEndereco()) {
            obterEndereco();//obtem
            enderecopessoaData.insertUpdate(enderecoPessoa);//salva
        }
        
        atualizarTabelaEndereco();
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
        
    }//GEN-LAST:event_btnSalvarFinalizarActionPerformed

    private void btnSalvarContinuar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarContinuar2ActionPerformed
        if (validarTelefone()) {
            Telefone telefone = obterTelefone();
            salvarTelefone(telefone);
            
        }
        
        atualizarTabelaTelefone();
        limparTelefone();
        
        //idTelefone = null;
        
        tabPrincipal.setEnabledAt(2,true);
        tabPrincipal.setSelectedIndex(2);
        atualizarTabelaEndereco();
        
    }//GEN-LAST:event_btnSalvarContinuar2ActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if(tblFornecedor.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Selecione um Fornecedor", "Alerta", 2);
	} else {
            Fornecedor f = (Fornecedor) tblFornecedor.getValueAt(tblFornecedor.getSelectedRow(), 0);
            atribuirFornecedor(f);
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void cbxUFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxUFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxUFActionPerformed

    private void txtNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroActionPerformed

    private void btnEditarEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarEnderecoActionPerformed
        if(tblFornecedorEndereco.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Selecione um Endereço", "Alerta", 2);
	} else {
            Enderecopessoa ep = (Enderecopessoa) tblFornecedorEndereco.getValueAt(tblFornecedorEndereco.getSelectedRow(), 0);
            atribuirEndereco(ep);
        }
    }//GEN-LAST:event_btnEditarEnderecoActionPerformed

    private void btnNovoEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoEnderecoActionPerformed
        limparEndereco();
        idEndereco = 0;
    }//GEN-LAST:event_btnNovoEnderecoActionPerformed

    private void txtCEPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCEPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCEPActionPerformed

    private void txtNumeroTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroTelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroTelefoneActionPerformed

    private void btnEditarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarTelefoneActionPerformed
        Object selectedObject = (Object) tblFornecedorTelefone.getModel().getValueAt(tblFornecedorTelefone.getSelectedRow(), 0);
        Object selectedObjectTipoTelefone = (Object) tblFornecedorTelefone.getModel().getValueAt(tblFornecedorTelefone.getSelectedRow(), 1);
        Telefone tel = Telefone.class.cast(selectedObject);
        Tipotelefone tipoTel = Tipotelefone.class.cast(selectedObjectTipoTelefone);
        
        telefone = tel;
        idTelefone = tel.getIdTelefone();
        
        Tipotelefone item = tipoTelefoneData.getTipotelefone(tipoTel.getIdTipoTelefone());
        
        cbxTipoTelefone.setSelectedItem(item);
        
        atribuirTelefone(tel);
        atualizarTabelaTelefone();
        
    }//GEN-LAST:event_btnEditarTelefoneActionPerformed

    private void btnExcluirTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirTelefoneActionPerformed
        if (tblFornecedorTelefone.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Selecione um Telefone", "Alerta", 2);
        } else {
            Object selectedObject = (Object) tblFornecedorTelefone.getModel().getValueAt(tblFornecedorTelefone.getSelectedRow(), 0);
            Telefone tel = Telefone.class.cast(selectedObject);

            telefoneData.deleteTelefone(tel);
            atualizarTabelaTelefone();
        }
        
    }//GEN-LAST:event_btnExcluirTelefoneActionPerformed

    private void btnNovoTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoTelefoneActionPerformed
        limparTelefone();
        idEndereco = 0;
    }//GEN-LAST:event_btnNovoTelefoneActionPerformed

    private void btnExcluirEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirEnderecoActionPerformed
        if (tblFornecedorEndereco.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Selecione um Endereço", "Alerta", 2);
        } else {
            Object selectedObject = (Object) tblFornecedorEndereco.getModel().getValueAt(tblFornecedorEndereco.getSelectedRow(), 0);
            Enderecopessoa end = Enderecopessoa.class.cast(selectedObject);

            enderecopessoaData.deleteEnderecoPessoa(end);
            atualizarTabelaEndereco();
        }

    }//GEN-LAST:event_btnExcluirEnderecoActionPerformed

    public void atualizarTabela() {
        tblFornecedor = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Nome", "CNPJ"}, 0) {
            private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        //tblFornecedor.setModel();
        model.setRowCount(0);
        List<Fornecedor> fornecedores = fornecedorData.selectFornecedor();
        for (Fornecedor f : fornecedores) {
            
            model.addRow(new Object[]{ 
                f, 
                f.getPessoajuridica().getPessoa().getNome(),
                f.getPessoajuridica().getCnpj()
            });
        }
        tblFornecedor.setModel(model);
       
        tblFornecedor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnEditarActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblFornecedor.setAutoCreateRowSorter(true);
        tblFornecedor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblFornecedor.getColumnModel().getColumn(0).setMinWidth(0);
        tblFornecedor.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblFornecedor.getColumnModel().getColumn(1).setPreferredWidth(280);
        tblFornecedor.getColumnModel().getColumn(2).setPreferredWidth(150);
        tblFornecedor.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane3.setViewportView(tblFornecedor);
    }
    
    public void atualizarTabelaEndereco() {
        tblFornecedorEndereco = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Logradouro", "Numero", "Bairro", "Cidade"}, 0) {
            private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        //tblFornecedor.setModel();
        model.setRowCount(0);
        List<Object[]> enderecos = enderecopessoaData.selectPorPessoa(fornecedor.getIdFornecedor());
        for (Object[] o : enderecos) {
            Enderecopessoa ep = (Enderecopessoa)o[0];
            Endereco e = (Endereco)o[1];
            Bairro b = (Bairro)o[2];
            Cidade c = (Cidade)o[3];
            //Bairro b = bairroData.getBairro(e.getBairro());
            model.addRow(new Object[]{ 
                ep, 
                e.getEnderecoCompleto(), //.getLogradouro mostrou só "Rua", e não "Rua Tal..."
                ep.getNumero(),
                b.getBairro(),
                c.getCidade()
            });
        }
        tblFornecedorEndereco.setModel(model);
       
        tblFornecedorEndereco.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnEditarEnderecoActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblFornecedorEndereco.setAutoCreateRowSorter(true);
        tblFornecedorEndereco.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblFornecedorEndereco.getColumnModel().getColumn(0).setMinWidth(0);
        tblFornecedorEndereco.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblFornecedorEndereco.getColumnModel().getColumn(1).setPreferredWidth(250);
        tblFornecedorEndereco.getColumnModel().getColumn(2).setPreferredWidth(150);
        tblFornecedorEndereco.getColumnModel().getColumn(3).setPreferredWidth(250);
        tblFornecedorEndereco.getColumnModel().getColumn(4).setPreferredWidth(240);
        tblFornecedorEndereco.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane6.setViewportView(tblFornecedorEndereco);
    }
    
    public void atualizarTabelaTelefone() {
        tblFornecedorTelefone = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"","","Telefone", "Operadora", "Tipo de Telefone"}, 0) {
            //1 telefone
            //2 tipo telefone
                       
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        
        
        model.setRowCount(0);
        //Object[] telefones = telefoneData.selectPorPessoa(cliente.getIdCliente()).toArray();
        
        List<Object[]> telefones = telefoneData.selectPorPessoa(fornecedor.getIdFornecedor());
        
        for (Object[] o : telefones)
        {
            Telefone t = (Telefone)o[0];
            Tipotelefone tp = (Tipotelefone)o[1];
            
            model.addRow(new Object[]{ 
                t,
                tp,
                t.getNumeroTelefone(),
                t.getOperadora(),
                tp.getDescricao()
            });
        }
        
        tblFornecedorTelefone.setModel(model);
       
        tblFornecedorTelefone.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnEditarTelefoneActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        
        tblFornecedorTelefone.setAutoCreateRowSorter(true);
        tblFornecedorTelefone.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblFornecedorTelefone.getColumnModel().getColumn(0).setMinWidth(0);
        tblFornecedorTelefone.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblFornecedorTelefone.getColumnModel().getColumn(1).setMinWidth(0);
        tblFornecedorTelefone.getColumnModel().getColumn(1).setPreferredWidth(0);
        tblFornecedorTelefone.getColumnModel().getColumn(2).setPreferredWidth(280);
        tblFornecedorTelefone.getColumnModel().getColumn(3).setPreferredWidth(200);
        tblFornecedorTelefone.getColumnModel().getColumn(4).setPreferredWidth(200);
        tblFornecedorTelefone.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane7.setViewportView(tblFornecedorTelefone);
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormCadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormCadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormCadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormCadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormCadastroFornecedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEditarEndereco;
    private javax.swing.JButton btnEditarTelefone;
    private javax.swing.JButton btnExcluirEndereco;
    private javax.swing.JButton btnExcluirTelefone;
    private javax.swing.JButton btnNovoEndereco;
    private javax.swing.JButton btnNovoEndereco1;
    private javax.swing.JButton btnNovoTelefone;
    private javax.swing.JButton btnSalvarContinuar1;
    private javax.swing.JButton btnSalvarContinuar2;
    private javax.swing.JButton btnSalvarFinalizar;
    private javax.swing.JComboBox cbxTipoTelefone;
    private javax.swing.JComboBox cbxUF;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTextArea jTextArea1;
    private java.awt.Label label1;
    private java.awt.Label label10;
    private java.awt.Label label11;
    private java.awt.Label label12;
    private java.awt.Label label13;
    private java.awt.Label label14;
    private java.awt.Label label15;
    private java.awt.Label label16;
    private java.awt.Label label17;
    private java.awt.Label label2;
    private java.awt.Label label6;
    private java.awt.Label label7;
    private java.awt.Label label8;
    private java.awt.Label label9;
    private javax.swing.JLabel lblBairro;
    private javax.swing.JLabel lblCEP;
    private javax.swing.JLabel lblCNPJFornecedor;
    private javax.swing.JLabel lblCidade;
    private javax.swing.JLabel lblComplemento;
    private javax.swing.JLabel lblEmailFornecedor;
    private javax.swing.JLabel lblInscriacaoEstadual;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblLogradouro;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNumero;
    private javax.swing.JLabel lblNumeroTelefone;
    private javax.swing.JLabel lblObservacao;
    private javax.swing.JLabel lblObservacaoFornecedorEnd;
    private javax.swing.JLabel lblOperadora;
    private javax.swing.JLabel lblRazaoSocial;
    private javax.swing.JLabel lblSiteFornecedor;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblTituloFornecedor;
    private javax.swing.JPanel tabEndereco;
    private javax.swing.JPanel tabFornecedor;
    private javax.swing.JTabbedPane tabPrincipal;
    private javax.swing.JPanel tabTelefone;
    private javax.swing.JTable tblFornecedor;
    private javax.swing.JTable tblFornecedorEndereco;
    private javax.swing.JTable tblFornecedorTelefone;
    private javax.swing.JTextField txtBairro;
    private javax.swing.JFormattedTextField txtCEP;
    private javax.swing.JTextField txtCNPJ;
    private javax.swing.JTextField txtCidade;
    private javax.swing.JTextField txtComplemento;
    private javax.swing.JTextField txtEmailFornecedor;
    private javax.swing.JTextField txtInscricaoEstadual;
    private javax.swing.JTextField txtLogradouro;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtNumero;
    private javax.swing.JFormattedTextField txtNumeroTelefone;
    private javax.swing.JTextArea txtObservacao;
    private javax.swing.JTextField txtOperadora;
    private javax.swing.JTextField txtRazaoSocial;
    private javax.swing.JTextField txtSiteFornecedor;
    // End of variables declaration//GEN-END:variables
}
