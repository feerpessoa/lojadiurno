/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Data.ClienteData;
import Data.EnderecoData;
import Data.EnderecoPessoaData;
import Data.TelefoneData;
import Data.TipoTelefoneData;
import business.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabriel
 */
public class TelaCliente extends javax.swing.JFrame {

    private Cliente cliente;
    private Tipotelefone tipotelefone;
    private Telefone telefone;
    private Integer idCliente = null;
    private Integer idTelefone = null;
    private int tipoDePessoa = -1;
    private Enderecopessoa enderecoPessoa;
    private EnderecoData enderecoData;
    private TipoTelefoneData tipoTelefoneData;
    private ClienteData clienteData;
    private TelefoneData telefoneData;
    private Endereco endereco;
    private int idEndereco;
     private EnderecoPessoaData enderecopessoaData;
    public TelaCliente()
    {
        initComponents();
        
        jtpCadastro.setSelectedIndex(0);
        //Dasabilita os outros tabs
        jtpCadastro.setEnabledAt(1, false);
        jtpCadastro.setEnabledAt(2, false);
        setEnabledTipoPessoa(0,false);
        setEnabledTipoPessoa(1,false);
        //Desabilitar componentes
        //btnCompletar.setEnabled(false);
        enderecoData = new EnderecoData();
        enderecopessoaData = new EnderecoPessoaData();
        tipoTelefoneData = new TipoTelefoneData();
        telefoneData = new TelefoneData();
        tipotelefone = new Tipotelefone();
        clienteData = new ClienteData();
        cbxTipoTelefone.removeAllItems();
        for (Tipotelefone t : tipoTelefoneData.selectTipotelefone()){
            cbxTipoTelefone.addItem(t);
        }
        Telefone t = new Telefone();
        t.setTipotelefone((Tipotelefone) cbxTipoTelefone.getSelectedItem());
        t.setPessoa(null);
        idCliente = 0;
    }
    public TelaCliente(int idcliente) // pode ser um ID também
    {
        super();
        initComponents();
        lblTitulo.setText("Editar Cliente");
        btnProximoAEnderecoETelefone.setEnabled(false);
        btnProximoAEnderecoETelefone.setVisible(false);
        //PuxarCliente
        
        idCliente = idcliente;
    }
    
    private void atribuirCliente(Cliente c)
    {
        idCliente = c.getIdCliente();
        txtNome.setText(c.getPessoa().getNome());
        txtEmail.setText(c.getPessoa().getEmail());
        tipoDePessoa = c.getPessoa().getPessoafisica() != null ? 0 : 1;
        if(tipoDePessoa == 0)
        {
            if(c.getPessoa().getPessoafisica().getSexo() == 'm' || c.getPessoa().getPessoafisica().getSexo() == 'M')
            {
                rdoMasc.setSelected(true);
            }
            else if(c.getPessoa().getPessoafisica().getSexo() == 'f' || c.getPessoa().getPessoafisica().getSexo() == 'F')
            {
                rdoFem.setSelected(true);
            }
            txtCpf.setText(c.getPessoa().getPessoafisica().getCpf());
            txtRG.setText(c.getPessoa().getPessoafisica().getRg());
            txtNascimento.setText(c.getPessoa().getPessoafisica().getDataNascimento().toString());
        }
        if(tipoDePessoa == 1)
        {
            txtRazaoSocial.setText(c.getPessoa().getPessoajuridica().getRazaoSocial());
            txtCnpj.setText(c.getPessoa().getPessoajuridica().getCnpj());
            txtInscricaoEstadual.setText(c.getPessoa().getPessoajuridica().getIncricaoEstadual());
        }
        
        //atribuir endereço
        //atribuir telefones
    }
    private void setEnabledTipoPessoa(int tabIndex, boolean tf)
    {
        if(tabIndex == 0)
        {
            rdoMasc.setEnabled(tf);
            rdoFem.setEnabled(tf);
            txtCpf.setEnabled(tf);
            txtRG.setEnabled(tf);
            txtNascimento.setEnabled(tf);
        }
        else if(tabIndex == 1)
        {
            txtRazaoSocial.setEnabled(tf);
            txtCnpj.setEnabled(tf);
            txtInscricaoEstadual.setEnabled(tf);
        }
    }
    
    private boolean validar()
    {
        if(txtNome.getText().equals(""))
        {
            JOptionPane.showMessageDialog(txtComplemento, "O nome do cliente deve ser informado.", "Nome não foi Informado", JOptionPane.ERROR_MESSAGE);
            
            return false;
        }
        if(tipoDePessoa == -1)
        {
            JOptionPane.showMessageDialog(txtComplemento, "Escolha um tipo de pessoa.", "Tipo de pessoa não informado", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        else
        {
            //TIPO DE PESSOA - PESSOA FÍSICA
            if(tipoDePessoa == 0)
            {
                if((rdoMasc.isSelected() || rdoFem.isSelected()) == false)
                {
                    JOptionPane.showMessageDialog(txtComplemento, "O sexo do cliente não foi informado.", "Escolha um Sexo", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                
                if(txtCpf.getText().equals(""))
                {
                    JOptionPane.showMessageDialog(txtComplemento, "CPF do cliente não foi informado.", "CPF não informado", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                
                if(txtNascimento.getText().equals(""))
                {
                    JOptionPane.showMessageDialog(txtComplemento, "Data de nascimento não doi informado.", "Nascimento não informado", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
            // TIPO DE PESSOA - PESSOA JURÍDICA
            if(tipoDePessoa == 1)
            {
                if(txtRazaoSocial.getText().equals(""))
                {
                    JOptionPane.showMessageDialog(txtComplemento, "Razão social do cliente não foi informado.", "Razão Social não Informado", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                
                if(txtCnpj.getText().equals(""))
                {
                    JOptionPane.showMessageDialog(txtComplemento, "CNPJ do cliente não foi informado.", "CNPJ não informado", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
            
        }
        
        //SE ESTIVER NA ABA DE ENDEREÇOS
        if(jtpCadastro.isEnabledAt(1) == true)
        {
            if(txtCEP.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "CEP deve ser informado.", "CEP não foi informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtEndereco.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "Endereço deve ser informado.", "Endereço nâo informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtNumeroDoEndereco.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "Numero do endereço deve ser informado.", "Numero do Endereço não infomado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtLogradouro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O logradouro deve ser informado.", "Logradouro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtBairro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O Bairro deve ser informado.", "Bairro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtCidade.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A cidade deve ser informado.", "Cidade não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(cbxUF.getSelectedIndex() == 0)
            {
                JOptionPane.showMessageDialog(txtComplemento, "A unidade federativa ou sigla do estado deve ser informado.", "Unidade Federativa não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
        }
        
        // SE ESTIVER NA ABA DE TELEFONES
        if(jtpCadastro.isEnabledAt(2) == true)
        {
            if(txtTelefone.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O numero telefônico não foi informado.", "Numero teleônico não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(txtOperadora.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A operadora do telefone não foi informado.", "Operadora não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(cbxTipoTelefone.getSelectedIndex() == 0)
            {
                JOptionPane.showMessageDialog(txtComplemento, "O tipo de telefone não foi informado.", "Tipo de Telefone não informado.", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        
        return true;
    }
    
     private boolean validarEndereco(){
        
        
         
            if(txtCEP.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "CEP deve ser informado.", "CEP não foi informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtNumeroDoEndereco.getText().equals("") )
            {
                JOptionPane.showMessageDialog(txtComplemento, "Numero do endereço deve ser informado.", "Numero do Endereço não infomado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtLogradouro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O logradouro deve ser informado.", "Logradouro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtBairro.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "O Bairro deve ser informado.", "Bairro não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
            if(txtCidade.getText().equals(""))
            {
                JOptionPane.showMessageDialog(txtComplemento, "A cidade deve ser informado.", "Cidade não informado", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            
        return true;
    }
     
      private void limparEndereco(){
         txtComplemento.setText("");
         txtNumeroDoEndereco.setText("");
         txtObservacao.setText("");
         txtLogradouro.setText("");
         txtCEP.setText("");
         txtCidade.setText("");
         txtBairro.setText("");
         cbxUF.setSelectedItem(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rdogPessoa = new javax.swing.ButtonGroup();
        rdogSexo = new javax.swing.ButtonGroup();
        jtpCadastro = new javax.swing.JTabbedPane();
        pnlDados = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        rdoFisica = new javax.swing.JRadioButton();
        rdoJuridica = new javax.swing.JRadioButton();
        jtpTipoPessoa = new javax.swing.JTabbedPane();
        pnlPessoa = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        rdoMasc = new javax.swing.JRadioButton();
        rdoFem = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtRG = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtNascimento = new javax.swing.JFormattedTextField();
        txtCpf = new javax.swing.JFormattedTextField();
        pnlJuridica = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtRazaoSocial = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtInscricaoEstadual = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtCnpj = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        btnProximoAEnderecoETelefone = new javax.swing.JButton();
        pnlEndereco = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtNumeroDoEndereco = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        btnBuscarPeloCEP = new javax.swing.JButton();
        txtEndereco = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txtBairro = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtLogradouro = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtCidade = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        cbxUF = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblEndereco = new javax.swing.JTable();
        jLabel27 = new javax.swing.JLabel();
        txtComplemento = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtObservacao = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        btnSalvarEndereco = new javax.swing.JButton();
        btnEditarEndereco = new javax.swing.JButton();
        btnRemoverEndereco = new javax.swing.JButton();
        jLabel32 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtCEP = new javax.swing.JFormattedTextField();
        pnlTelefone = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txtOperadora = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        cbxTipoTelefone = new javax.swing.JComboBox();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblTelefone = new javax.swing.JTable();
        btnSalvarTelefone = new javax.swing.JButton();
        btnEditarTelefone = new javax.swing.JButton();
        btnExcluirTelefone = new javax.swing.JButton();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        txtTelefone = new javax.swing.JFormattedTextField();
        lblLogo = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        lblTitulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Nome:");

        jLabel2.setText("Email:");

        jLabel3.setText("Tipo:");

        rdogPessoa.add(rdoFisica);
        rdoFisica.setText("Pessoa Física");
        rdoFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoFisicaActionPerformed(evt);
            }
        });

        rdogPessoa.add(rdoJuridica);
        rdoJuridica.setText("Pessoa Jurídica");
        rdoJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoJuridicaActionPerformed(evt);
            }
        });

        jLabel4.setText("Sexo:");

        rdogSexo.add(rdoMasc);
        rdoMasc.setText("Masculino");

        rdogSexo.add(rdoFem);
        rdoFem.setText("Feminino");

        jLabel5.setText("CPF:");

        jLabel6.setText("Nascimento:");

        jLabel7.setText("RG:");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 51));
        jLabel15.setText("*");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 0, 51));
        jLabel16.setText("*");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 0, 51));
        jLabel17.setText("*");

        try {
            txtNascimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtNascimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNascimentoActionPerformed(evt);
            }
        });

        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCpf.setName(""); // NOI18N

        javax.swing.GroupLayout pnlPessoaLayout = new javax.swing.GroupLayout(pnlPessoa);
        pnlPessoa.setLayout(pnlPessoaLayout);
        pnlPessoaLayout.setHorizontalGroup(
            pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPessoaLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPessoaLayout.createSequentialGroup()
                        .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtRG, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
                            .addComponent(txtNascimento)
                            .addComponent(txtCpf))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlPessoaLayout.createSequentialGroup()
                        .addComponent(rdoMasc)
                        .addGap(30, 30, 30)
                        .addComponent(rdoFem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(28, 28, 28))
        );
        pnlPessoaLayout.setVerticalGroup(
            pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPessoaLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(rdoMasc)
                    .addComponent(rdoFem)
                    .addComponent(jLabel17))
                .addGap(18, 18, 18)
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel15)
                    .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel16)
                    .addComponent(txtNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jtpTipoPessoa.addTab("Pessoa Fisica", pnlPessoa);

        jLabel8.setText("Razão Social:");

        jLabel9.setText("CNPJ:");

        jLabel10.setText("Inscrição Estadual:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 0, 51));
        jLabel18.setText("*");

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 0, 51));
        jLabel19.setText("*");

        try {
            txtCnpj.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout pnlJuridicaLayout = new javax.swing.GroupLayout(pnlJuridica);
        pnlJuridica.setLayout(pnlJuridicaLayout);
        pnlJuridicaLayout.setHorizontalGroup(
            pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlJuridicaLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlJuridicaLayout.createSequentialGroup()
                        .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(39, 39, 39))
                    .addGroup(pnlJuridicaLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(12, 12, 12)))
                .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtInscricaoEstadual, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .addComponent(txtRazaoSocial, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCnpj))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        pnlJuridicaLayout.setVerticalGroup(
            pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlJuridicaLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGap(18, 18, 18)
                .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(jLabel19))
                    .addComponent(txtCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtInscricaoEstadual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(54, Short.MAX_VALUE))
        );

        jtpTipoPessoa.addTab("Pessoa Jurídica", pnlJuridica);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Add User-100.png"))); // NOI18N

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 0, 51));
        jLabel13.setText("*");

        jLabel14.setText("Observações");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        btnProximoAEnderecoETelefone.setText("Próximo");
        btnProximoAEnderecoETelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProximoAEnderecoETelefoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDadosLayout = new javax.swing.GroupLayout(pnlDados);
        pnlDados.setLayout(pnlDadosLayout);
        pnlDadosLayout.setHorizontalGroup(
            pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDadosLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDadosLayout.createSequentialGroup()
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlDadosLayout.createSequentialGroup()
                                .addComponent(rdoFisica)
                                .addGap(18, 18, 18)
                                .addComponent(rdoJuridica))
                            .addComponent(txtNome, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
                            .addComponent(txtEmail))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 351, Short.MAX_VALUE))
                    .addGroup(pnlDadosLayout.createSequentialGroup()
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jtpTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 576, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlDadosLayout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDadosLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnProximoAEnderecoETelefone)
                                .addGap(58, 58, 58))
                            .addGroup(pnlDadosLayout.createSequentialGroup()
                                .addGap(101, 101, 101)
                                .addComponent(jLabel11)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        pnlDadosLayout.setVerticalGroup(
            pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDadosLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDadosLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(rdoFisica)
                            .addComponent(rdoJuridica))
                        .addGap(18, 18, 18)
                        .addComponent(jtpTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addGroup(pnlDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addContainerGap(28, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDadosLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addGap(153, 153, 153)
                        .addComponent(btnProximoAEnderecoETelefone)
                        .addGap(33, 33, 33))))
        );

        jtpCadastro.addTab("Dados Principais", pnlDados);

        jLabel20.setText("CEP:");

        jLabel21.setText("Número:");

        jLabel22.setText("Endereço:");

        btnBuscarPeloCEP.setText("Buscar");
        btnBuscarPeloCEP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPeloCEPActionPerformed(evt);
            }
        });

        txtEndereco.setEnabled(false);

        jLabel23.setText("Bairro:");

        txtBairro.setEnabled(false);

        jLabel24.setText("Logradouro:");

        txtLogradouro.setEnabled(false);

        jLabel25.setText("Cidade:");

        txtCidade.setEnabled(false);

        jLabel26.setText("UF:");

        cbxUF.setEnabled(false);
        cbxUF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxUFActionPerformed(evt);
            }
        });

        tblEndereco.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblEndereco);

        jLabel27.setText("Complemento:");

        jLabel28.setText("Observação:");

        txtObservacao.setColumns(20);
        txtObservacao.setRows(5);
        jScrollPane3.setViewportView(txtObservacao);

        btnSalvarEndereco.setText("Salvar Endereço");
        btnSalvarEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarEnderecoActionPerformed(evt);
            }
        });

        btnEditarEndereco.setText("Editar Endereço");
        btnEditarEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarEnderecoActionPerformed(evt);
            }
        });

        btnRemoverEndereco.setText("Remover Endereço");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 0, 51));
        jLabel32.setText("*");

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 0, 51));
        jLabel41.setText("*");

        try {
            txtCEP.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout pnlEnderecoLayout = new javax.swing.GroupLayout(pnlEndereco);
        pnlEndereco.setLayout(pnlEnderecoLayout);
        pnlEnderecoLayout.setHorizontalGroup(
            pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 697, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSeparator1)
                            .addComponent(btnRemoverEndereco, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                            .addComponent(btnEditarEndereco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSalvarEndereco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 8, Short.MAX_VALUE))
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23)
                            .addComponent(jLabel25))
                        .addGap(27, 27, 27)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(82, 82, 82)
                                .addComponent(jLabel26)
                                .addGap(10, 10, 10)
                                .addComponent(cbxUF, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                .addComponent(txtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBuscarPeloCEP)
                                .addGap(35, 35, 35)
                                .addComponent(jLabel21)
                                .addGap(18, 18, 18)
                                .addComponent(txtNumeroDoEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                        .addComponent(txtBairro)
                                        .addGap(35, 35, 35)
                                        .addComponent(jLabel24)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtEndereco))
                                .addGap(18, 18, 18)))
                        .addGap(18, 18, 18)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel28)
                            .addComponent(jLabel27))
                        .addGap(25, 25, 25)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtComplemento)
                            .addComponent(jScrollPane3))))
                .addContainerGap())
        );
        pnlEnderecoLayout.setVerticalGroup(
            pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel20))
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(btnBuscarPeloCEP)
                            .addComponent(jLabel32)
                            .addComponent(txtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNumeroDoEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel27)
                            .addComponent(txtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel41))))
                .addGap(18, 18, 18)
                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel22))
                            .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel28)))
                        .addGap(18, 18, 18)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23)
                                    .addComponent(jLabel24))))
                        .addGap(18, 18, 18)
                        .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxUF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlEnderecoLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel25)
                                    .addComponent(jLabel26)))))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(pnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))
                    .addGroup(pnlEnderecoLayout.createSequentialGroup()
                        .addComponent(btnSalvarEndereco)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEditarEndereco)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRemoverEndereco)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(74, 74, 74))))
        );

        jtpCadastro.addTab("Endereços", pnlEndereco);

        jLabel29.setText("Numero de Telefone:");

        jLabel30.setText("Operadora:");

        jLabel31.setText("Tipo de Telefone:");

        tblTelefone.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tblTelefone);

        btnSalvarTelefone.setText("Salvar Telefone");
        btnSalvarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarTelefoneActionPerformed(evt);
            }
        });

        btnEditarTelefone.setText("Editar Telefone");
        btnEditarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarTelefoneActionPerformed(evt);
            }
        });

        btnExcluirTelefone.setText("Excluir Telefone");
        btnExcluirTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirTelefoneActionPerformed(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 0, 51));
        jLabel38.setText("*");

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 0, 51));
        jLabel39.setText("*");

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 0, 51));
        jLabel40.setText("*");

        try {
            txtTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout pnlTelefoneLayout = new javax.swing.GroupLayout(pnlTelefone);
        pnlTelefone.setLayout(pnlTelefoneLayout);
        pnlTelefoneLayout.setHorizontalGroup(
            pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTelefoneLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlTelefoneLayout.createSequentialGroup()
                        .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel29)
                            .addComponent(jLabel30)
                            .addComponent(jLabel31))
                        .addGap(34, 34, 34)
                        .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtOperadora)
                            .addComponent(cbxTipoTelefone, 0, 231, Short.MAX_VALUE)
                            .addComponent(txtTelefone))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlTelefoneLayout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE)
                        .addGap(46, 46, 46)
                        .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnEditarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSalvarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnExcluirTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42))))
        );
        pnlTelefoneLayout.setVerticalGroup(
            pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTelefoneLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel38)
                    .addComponent(txtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtOperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addGap(18, 18, 18)
                .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(cbxTipoTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40))
                .addGap(18, 18, 18)
                .addGroup(pnlTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
                    .addGroup(pnlTelefoneLayout.createSequentialGroup()
                        .addComponent(btnSalvarTelefone)
                        .addGap(18, 18, 18)
                        .addComponent(btnEditarTelefone)
                        .addGap(18, 18, 18)
                        .addComponent(btnExcluirTelefone)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jtpCadastro.addTab("Telefones", pnlTelefone);

        lblLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/icon.jpg"))); // NOI18N

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 0, 51));
        jLabel12.setText("* Campos Obrigatorio");

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/solutionItems/Delete Sign-26.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        lblTitulo.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo.setText("Cadastro de Cliente");
        lblTitulo.setToolTipText("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addGap(20, 20, 20))
                    .addComponent(jtpCadastro)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblLogo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTitulo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblLogo)
                    .addComponent(lblTitulo))
                .addGap(18, 18, 18)
                .addComponent(jtpCadastro)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(jLabel12))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rdoFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoFisicaActionPerformed
        setEnabledTipoPessoa(0,true);
        setEnabledTipoPessoa(1,false);
        tipoDePessoa = 0;
    }//GEN-LAST:event_rdoFisicaActionPerformed

    private void rdoJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoJuridicaActionPerformed
        setEnabledTipoPessoa(1,true);
        setEnabledTipoPessoa(0,false);
        tipoDePessoa = 1;
    }//GEN-LAST:event_rdoJuridicaActionPerformed

    private void btnProximoAEnderecoETelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProximoAEnderecoETelefoneActionPerformed
        salvarCliente();
        
        if(validar())
        {
            jtpCadastro.setEnabledAt(1, true);
            jtpCadastro.setEnabledAt(2, true);
            //btnCompletar.setEnabled(true);
            jtpCadastro.setSelectedIndex(1);
        }
        
        
    }//GEN-LAST:event_btnProximoAEnderecoETelefoneActionPerformed

    private void obterCliente() {
        Pessoa pessoa = new Pessoa();
        pessoa.setIdPessoa(idCliente);
        pessoa.setEmail(txtEmail.getText());
        pessoa.setNome(txtNome.getText());
        pessoa.setObservacao(jTextArea1.getText());

        //MONTAR OBJETO PESSOA FÍSICA
        if(tipoDePessoa == 0)
        {
            Pessoafisica pf = new Pessoafisica();
            pf.setCpf(txtCpf.getText());

            pf.setIdPessoaFisica(idCliente);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            Date inputDate = null;

            try
            {
                inputDate = dateFormat.parse(txtNascimento.getText());
            }

            catch(Exception ex)
            {
                ex.printStackTrace();
            }

            pf.setDataNascimento(inputDate);
            pf.setRg(txtRG.getText());

            if(rdoMasc.isSelected())
            {
                pf.setSexo('M');
            }

            else
            {
                pf.setSexo('F');
            }

            pessoa.setPessoafisica(pf);
        }

        //MONTAR OBJETO PESSOA JURÍDICA
        if(tipoDePessoa == 1)
        {
            Pessoajuridica pj = new Pessoajuridica();
            pj.setCnpj(txtCnpj.getText());
            pj.setIdPessoaJuridica(idCliente);
            pj.setIncricaoEstadual(txtInscricaoEstadual.getText());
            pj.setRazaoSocial(txtRazaoSocial.getText());

            pessoa.setPessoajuridica(pj);
        }

        cliente = new Cliente(pessoa);
        cliente.setIdCliente(idCliente);
    }
    
    private void obterTelefone()
    {
        cliente.getPessoa().setIdPessoa(cliente.getIdCliente());
        
        telefone.setPessoa(cliente.getPessoa());
        telefone.setNumeroTelefone(txtTelefone.getText());
        telefone.setOperadora(Short.parseShort(txtOperadora.getText()));
        telefone.setTipotelefone((Tipotelefone)cbxTipoTelefone.getSelectedItem());
    }
    
    private boolean validaTelefone()
    {
        if(txtTelefone.getText() == null ||  txtTelefone.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(this,"Informe um numero para o campo");
            return false;
        }
        
        if(txtOperadora.getText() == null || txtOperadora.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(this, "Informe a operadora");
            return false;
        }
        
        return true;
    }
    
    private void salvarTelefone(Integer idTelefone)
    {
        if(idTelefone == null)
        {
            telefone = new Telefone();
            obterTelefone();
        }
        
        else
        {
            obterTelefone();
        }
        
        
        telefoneData.saveTelefone(telefone);
    }
    
    private void salvarCliente()
    {
        obterCliente();
        
        if(cliente.getIdCliente() == 0)
        {
            if (tipoDePessoa == 0) {

                cliente.setIdCliente(null);
                cliente.getPessoa().setIdPessoa(null);
                cliente.getPessoa().getPessoafisica().setIdPessoaFisica(null);
            }
            
            if (tipoDePessoa == 1) {
                cliente.setIdCliente(null);
                cliente.getPessoa().setIdPessoa(null);
                cliente.getPessoa().getPessoajuridica().setIdPessoaJuridica(null);
            }
        }
        
        if(validar())
        {
            if (tipoDePessoa == 0) {
                clienteData.salvarClientePessoaFisica(cliente);
                idCliente = cliente.getIdCliente();
            } 
            
            if (tipoDePessoa == 1) {
                clienteData.salvarClientePessoaJuridica(cliente);
                idCliente = cliente.getIdCliente();
            }
                       
            JOptionPane.showMessageDialog(txtComplemento, "Ação concluída com sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
            
            try
            {
                this.finalize();
            }
            
            catch (Throwable ex)
            {
                Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void atualizarTabelaTelefone() {
        tblTelefone = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"","","Telefone", "Operadora", "Tipo de Telefone"}, 0) {
            //1 telefone
            //2 tipo telefone
            
            
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        
        
        model.setRowCount(0);
        //Object[] telefones = telefoneData.selectPorPessoa(cliente.getIdCliente()).toArray();
        
        List<Object[]> telefones = telefoneData.selectPorPessoa(cliente.getIdCliente());
        
        for (Object[] o : telefones)
        {
            Telefone t = (Telefone)o[0];
            Tipotelefone tp = (Tipotelefone)o[1];
            
            model.addRow(new Object[]{ 
                t,
                tp,
                t.getNumeroTelefone(),
                t.getOperadora(),
                tp.getDescricao()
            });
        }
        
        tblTelefone.setModel(model);
       
        tblTelefone.setAutoCreateRowSorter(true);
        tblTelefone.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblTelefone.getColumnModel().getColumn(0).setMinWidth(0);
        tblTelefone.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblTelefone.getColumnModel().getColumn(1).setMinWidth(0);
        tblTelefone.getColumnModel().getColumn(1).setPreferredWidth(0);
        tblTelefone.getColumnModel().getColumn(2).setPreferredWidth(280);
        tblTelefone.getColumnModel().getColumn(3).setPreferredWidth(150);
        tblTelefone.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane4.setViewportView(tblTelefone);
        
    }
    
    private void btnBuscarPeloCEPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPeloCEPActionPerformed
        Endereco endereco = enderecoData.getEnderecoCep(txtCEP.getText());
        
        txtEndereco.setText(endereco.getEnderecoCompleto());
        txtBairro.setText(endereco.getBairro().getBairro());
        txtCidade.setText(endereco.getBairro().getCidade().getCidade());
        txtNumeroDoEndereco.setText("");
        cbxUF.addItem(endereco.getBairro().getCidade().getEstado().getUf());
        cbxUF.setSelectedItem(endereco.getBairro().getCidade().getEstado().getUf());
        txtLogradouro.setText(endereco.getLogradouro());         
    }//GEN-LAST:event_btnBuscarPeloCEPActionPerformed

    public void limparFormulario()
    {
        txtEmail.setText("");
        txtNome.setText("");
        jTextArea1.setText("");
        txtCpf.setText("");
        txtRG.setText("");
        txtNascimento.setText("");
        txtRazaoSocial.setText("");
        txtCnpj.setText("");
        txtInscricaoEstadual.setText("");
           
    }
    
    public void atribuirTelefone(Telefone t)
    {
        txtTelefone.setText(t.getNumeroTelefone());
        txtOperadora.setText(String.valueOf(t.getOperadora()));
        
        cbxTipoTelefone.setSelectedItem(t);
    }
    
    public void limparTelefone()
    {
        txtTelefone.setText("");
        txtOperadora.setText("");
        cbxTipoTelefone.setSelectedIndex(0);
    }
    
    public void atualizarTabelaEndereco() {
        tblEndereco = new JTable();
        DefaultTableModel model = new DefaultTableModel(new Object[]{"", "Logradouro", "Numero", "Bairro", "Cidade"}, 0) {
            //private static final long serialVersionUID = 1L;
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        
        model.setRowCount(0);
        List<Object[]> enderecos = enderecopessoaData.selectPorPessoa(cliente.getIdCliente());
        for (Object[] o : enderecos) {
            Enderecopessoa ep = (Enderecopessoa)o[0];
            Endereco e = (Endereco)o[1];
            Bairro b = (Bairro)o[2];
            Cidade c = (Cidade)o[3];
            
            model.addRow(new Object[]{ 
                ep, 
                e.getEnderecoCompleto(), 
                ep.getNumero(),
                b.getBairro(),
                c.getCidade()
            });
        }
        tblEndereco.setModel(model);
       
        tblEndereco.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    //btnEditarEnderecoActionPerformed(new ActionEvent(this, 0, null));
                }
            }
        });
        tblEndereco.setAutoCreateRowSorter(true);
        tblEndereco.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblEndereco.getColumnModel().getColumn(0).setMinWidth(0);
        tblEndereco.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblEndereco.getColumnModel().getColumn(1).setPreferredWidth(250);
        tblEndereco.getColumnModel().getColumn(2).setPreferredWidth(150);
        tblEndereco.getColumnModel().getColumn(3).setPreferredWidth(250);
        tblEndereco.getColumnModel().getColumn(4).setPreferredWidth(240);
        tblEndereco.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane2.setViewportView(tblEndereco);
    }
    
    private void obterEndereco(){
         
         enderecoPessoa = new Enderecopessoa();
         enderecoPessoa.setId(new EnderecopessoaId(endereco.getIdEndereco(), cliente.getIdCliente()));
         enderecoPessoa.setPessoa(cliente.getPessoa());
         enderecoPessoa.setEndereco(endereco);
         enderecoPessoa.setComplemeto(txtComplemento.getText());
         enderecoPessoa.setNumero(Short.parseShort(txtNumeroDoEndereco.getText()));
         enderecoPessoa.setObservacao(txtObservacao.getText());
        
     }  
    
    
    private void atribuirEndereco(Enderecopessoa e){
         idEndereco = e.getId().getIdEndereco();
         txtComplemento.setText(e.getComplemeto());
         txtNumeroDoEndereco.setText(String.valueOf(e.getNumero()));
         txtObservacao.setText(e.getObservacao());
         txtLogradouro.setText(e.getEndereco().getEnderecoCompleto()); //se for getLogradouro() ele pega só "Rua" ao invés de "Rua Tal..."
         txtCEP.setText(e.getEndereco().getCep());
         txtCidade.setText(e.getEndereco().getBairro().getCidade().getCidade());
         txtBairro.setText(e.getEndereco().getBairro().getBairro());
         cbxUF.setSelectedItem(e.getEndereco().getBairro().getCidade().getEstado().getUf());
     }
    
    private void btnSalvarEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarEnderecoActionPerformed
        obterEndereco();
        enderecopessoaData.insertUpdate(enderecoPessoa);
        atualizarTabelaEndereco();
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
        
        
    }//GEN-LAST:event_btnSalvarEnderecoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        System.exit(0);
        
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnSalvarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarTelefoneActionPerformed
        
        salvarTelefone(idTelefone);
        atualizarTabelaTelefone();
        limparTelefone();
        idTelefone = null;
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!", "Êxito", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnSalvarTelefoneActionPerformed

    private void cbxUFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxUFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxUFActionPerformed

    private void txtNascimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNascimentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNascimentoActionPerformed

    private void btnEditarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarTelefoneActionPerformed
        Object selectedObject = (Object) tblTelefone.getModel().getValueAt(tblTelefone.getSelectedRow(), 0);
        Object selectedObjectTipoTelefone = (Object) tblTelefone.getModel().getValueAt(tblTelefone.getSelectedRow(), 1);
        
        Telefone tel = Telefone.class.cast(selectedObject);
        Tipotelefone tipoTel = Tipotelefone.class.cast(selectedObjectTipoTelefone);
        
        
        telefone = tel;
        idTelefone = tel.getIdTelefone();
        
        Tipotelefone item = tipoTelefoneData.getTipotelefone(tipoTel.getIdTipoTelefone());
        
        cbxTipoTelefone.setSelectedItem(item);
        
        atribuirTelefone(tel);
        
        atualizarTabelaTelefone();
        
    }//GEN-LAST:event_btnEditarTelefoneActionPerformed

    private void btnExcluirTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirTelefoneActionPerformed
        Object selectedObject = (Object) tblTelefone.getModel().getValueAt(tblTelefone.getSelectedRow(), 0);
        
        Telefone tel = Telefone.class.cast(selectedObject);
        
        JOptionPane.showMessageDialog(null, "Tem certeza disso?", "Alerta", 2);
        telefoneData.deleteTelefone(tel);
        
        atualizarTabelaTelefone();
        limparTelefone();
    }//GEN-LAST:event_btnExcluirTelefoneActionPerformed

    private void btnEditarEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarEnderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEditarEnderecoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarPeloCEP;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditarEndereco;
    private javax.swing.JButton btnEditarTelefone;
    private javax.swing.JButton btnExcluirTelefone;
    private javax.swing.JButton btnProximoAEnderecoETelefone;
    private javax.swing.JButton btnRemoverEndereco;
    private javax.swing.JButton btnSalvarEndereco;
    private javax.swing.JButton btnSalvarTelefone;
    private javax.swing.JComboBox cbxTipoTelefone;
    private javax.swing.JComboBox cbxUF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTabbedPane jtpCadastro;
    private javax.swing.JTabbedPane jtpTipoPessoa;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JPanel pnlDados;
    private javax.swing.JPanel pnlEndereco;
    private javax.swing.JPanel pnlJuridica;
    private javax.swing.JPanel pnlPessoa;
    private javax.swing.JPanel pnlTelefone;
    private javax.swing.JRadioButton rdoFem;
    private javax.swing.JRadioButton rdoFisica;
    private javax.swing.JRadioButton rdoJuridica;
    private javax.swing.JRadioButton rdoMasc;
    private javax.swing.ButtonGroup rdogPessoa;
    private javax.swing.ButtonGroup rdogSexo;
    private javax.swing.JTable tblEndereco;
    private javax.swing.JTable tblTelefone;
    private javax.swing.JTextField txtBairro;
    private javax.swing.JFormattedTextField txtCEP;
    private javax.swing.JTextField txtCidade;
    private javax.swing.JFormattedTextField txtCnpj;
    private javax.swing.JTextField txtComplemento;
    private javax.swing.JFormattedTextField txtCpf;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEndereco;
    private javax.swing.JTextField txtInscricaoEstadual;
    private javax.swing.JTextField txtLogradouro;
    private javax.swing.JFormattedTextField txtNascimento;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtNumeroDoEndereco;
    private javax.swing.JTextArea txtObservacao;
    private javax.swing.JTextField txtOperadora;
    private javax.swing.JTextField txtRG;
    private javax.swing.JTextField txtRazaoSocial;
    private javax.swing.JFormattedTextField txtTelefone;
    // End of variables declaration//GEN-END:variables
}
