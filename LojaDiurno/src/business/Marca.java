package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "marca", catalog = "lojadiurno"
)
public class Marca implements java.io.Serializable {

    private Short idMarca;
    private String descricao;
    private Set<Produto> produtos = new HashSet<Produto>(0);

    public Marca() {
    }

    public Marca(String descricao) {
        this.descricao = descricao;
    }

    public Marca(String descricao, Set<Produto> produtos) {
        this.descricao = descricao;
        this.produtos = produtos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdMarca", unique = true, nullable = false)
    public Short getIdMarca() {
        return this.idMarca;
    }

    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    @Column(name = "Descricao", nullable = false, length = 50)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "marca")
    public Set<Produto> getProdutos() {
        return this.produtos;
    }

    public void setProdutos(Set<Produto> produtos) {
        this.produtos = produtos;
    }

}
