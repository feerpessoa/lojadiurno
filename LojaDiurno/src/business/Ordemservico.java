package business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ordemservico", catalog = "lojadiurno"
)
public class Ordemservico implements java.io.Serializable {

    private Integer idOrdemServico;
    private Cliente cliente;
    private Pagamento pagamento;
    private Tipoordemservico tipoordemservico;
    private Vendedor vendedor;
    private int numeroOrdemServico;
    private Date data;
    private BigDecimal desconto;
    private String descricao;
    private BigDecimal valorServico;
    private Set<Notafiscal> notafiscals = new HashSet<Notafiscal>(0);
    private Set<Itemordemservico> itemordemservicos = new HashSet<Itemordemservico>(0);

    public Ordemservico() {
    }

    public Ordemservico(Pagamento pagamento, int numeroOrdemServico, Date data, String descricao, BigDecimal valorServico) {
        this.pagamento = pagamento;
        this.numeroOrdemServico = numeroOrdemServico;
        this.data = data;
        this.descricao = descricao;
        this.valorServico = valorServico;
    }

    public Ordemservico(Cliente cliente, Pagamento pagamento, Tipoordemservico tipoordemservico, Vendedor vendedor, int numeroOrdemServico, Date data, BigDecimal desconto, String descricao, BigDecimal valorServico, Set<Notafiscal> notafiscals, Set<Itemordemservico> itemordemservicos) {
        this.cliente = cliente;
        this.pagamento = pagamento;
        this.tipoordemservico = tipoordemservico;
        this.vendedor = vendedor;
        this.numeroOrdemServico = numeroOrdemServico;
        this.data = data;
        this.desconto = desconto;
        this.descricao = descricao;
        this.valorServico = valorServico;
        this.notafiscals = notafiscals;
        this.itemordemservicos = itemordemservicos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdOrdemServico", unique = true, nullable = false)
    public Integer getIdOrdemServico() {
        return this.idOrdemServico;
    }

    public void setIdOrdemServico(Integer idOrdemServico) {
        this.idOrdemServico = idOrdemServico;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdCliente")
    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdTipoPagamento", nullable = false)
    public Pagamento getPagamento() {
        return this.pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdTipoOrdemServico")
    public Tipoordemservico getTipoordemservico() {
        return this.tipoordemservico;
    }

    public void setTipoordemservico(Tipoordemservico tipoordemservico) {
        this.tipoordemservico = tipoordemservico;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdVendedor")
    public Vendedor getVendedor() {
        return this.vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @Column(name = "NumeroOrdemServico", nullable = false)
    public int getNumeroOrdemServico() {
        return this.numeroOrdemServico;
    }

    public void setNumeroOrdemServico(int numeroOrdemServico) {
        this.numeroOrdemServico = numeroOrdemServico;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Data", nullable = false, length = 19)
    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Column(name = "Desconto", precision = 10)
    public BigDecimal getDesconto() {
        return this.desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    @Column(name = "Descricao", nullable = false, length = 65535)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "ValorServico", nullable = false, precision = 10)
    public BigDecimal getValorServico() {
        return this.valorServico;
    }

    public void setValorServico(BigDecimal valorServico) {
        this.valorServico = valorServico;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ordemservico")
    public Set<Notafiscal> getNotafiscals() {
        return this.notafiscals;
    }

    public void setNotafiscals(Set<Notafiscal> notafiscals) {
        this.notafiscals = notafiscals;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ordemservico")
    public Set<Itemordemservico> getItemordemservicos() {
        return this.itemordemservicos;
    }

    public void setItemordemservicos(Set<Itemordemservico> itemordemservicos) {
        this.itemordemservicos = itemordemservicos;
    }

}
