package business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "foto", catalog = "lojadiurno"
)
public class Foto implements java.io.Serializable {

    private Short idFoto;
    private Produto produto;
    private String nomeFisico;
    private String caminho;

    public Foto() {
    }

    public Foto(Produto produto, String nomeFisico, String caminho) {
        this.produto = produto;
        this.nomeFisico = nomeFisico;
        this.caminho = caminho;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdFoto", unique = true, nullable = false)
    public Short getIdFoto() {
        return this.idFoto;
    }

    public void setIdFoto(Short idFoto) {
        this.idFoto = idFoto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Column(name = "NomeFisico", nullable = false, length = 50)
    public String getNomeFisico() {
        return this.nomeFisico;
    }

    public void setNomeFisico(String nomeFisico) {
        this.nomeFisico = nomeFisico;
    }

    @Column(name = "Caminho", nullable = false)
    public String getCaminho() {
        return this.caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

}
