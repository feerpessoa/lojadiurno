package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bairro", catalog = "lojadiurno"
)
public class Bairro implements java.io.Serializable {

    private Integer idBairro;
    private Cidade cidade;
    private String bairro;
    private Set<Endereco> enderecos = new HashSet<Endereco>(0);

    public Bairro() {
    }

    public Bairro(Cidade cidade, String bairro) {
        this.cidade = cidade;
        this.bairro = bairro;
    }

    public Bairro(Cidade cidade, String bairro, Set<Endereco> enderecos) {
        this.cidade = cidade;
        this.bairro = bairro;
        this.enderecos = enderecos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdBairro", unique = true, nullable = false)
    public Integer getIdBairro() {
        return this.idBairro;
    }

    public void setIdBairro(Integer idBairro) {
        this.idBairro = idBairro;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdCidade", nullable = false)
    public Cidade getCidade() {
        return this.cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @Column(name = "Bairro", nullable = false, length = 200)
    public String getBairro() {
        return this.bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bairro")
    public Set<Endereco> getEnderecos() {
        return this.enderecos;
    }

    public void setEnderecos(Set<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

}
