package business;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "movimentacao", catalog = "lojadiurno"
)
public class Movimentacao implements java.io.Serializable {

    private Integer idMovimentacao;
    private Produto produto;
    private Tipomovimentacao tipomovimentacao;
    private Date dataMovimentacao;
    private BigDecimal quantidade;

    public Movimentacao() {
    }

    public Movimentacao(Produto produto, Tipomovimentacao tipomovimentacao, Date dataMovimentacao, BigDecimal quantidade) {
        this.produto = produto;
        this.tipomovimentacao = tipomovimentacao;
        this.dataMovimentacao = dataMovimentacao;
        this.quantidade = quantidade;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdMovimentacao", unique = true, nullable = false)
    public Integer getIdMovimentacao() {
        return this.idMovimentacao;
    }

    public void setIdMovimentacao(Integer idMovimentacao) {
        this.idMovimentacao = idMovimentacao;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdTipoMovimentacao", nullable = false)
    public Tipomovimentacao getTipomovimentacao() {
        return this.tipomovimentacao;
    }

    public void setTipomovimentacao(Tipomovimentacao tipomovimentacao) {
        this.tipomovimentacao = tipomovimentacao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DataMovimentacao", nullable = false, length = 19)
    public Date getDataMovimentacao() {
        return this.dataMovimentacao;
    }

    public void setDataMovimentacao(Date dataMovimentacao) {
        this.dataMovimentacao = dataMovimentacao;
    }

    @Column(name = "Quantidade", nullable = false, precision = 10)
    public BigDecimal getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

}
