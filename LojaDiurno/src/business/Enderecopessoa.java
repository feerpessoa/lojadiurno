package business;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "enderecopessoa", catalog = "lojadiurno"
)
public class Enderecopessoa implements java.io.Serializable {

    private EnderecopessoaId id;
    private Endereco endereco;
    private Pessoa pessoa;
    private short numero;
    private String complemeto;
    private String observacao;

    public Enderecopessoa() {
    }

    public Enderecopessoa(EnderecopessoaId id, Endereco endereco, Pessoa pessoa, short numero) {
        this.id = id;
        this.endereco = endereco;
        this.pessoa = pessoa;
        this.numero = numero;
    }

    public Enderecopessoa(EnderecopessoaId id, Endereco endereco, Pessoa pessoa, short numero, String complemeto, String observacao) {
        this.id = id;
        this.endereco = endereco;
        this.pessoa = pessoa;
        this.numero = numero;
        this.complemeto = complemeto;
        this.observacao = observacao;
    }

    @EmbeddedId

    @AttributeOverrides({
        @AttributeOverride(name = "idEndereco", column = @Column(name = "IdEndereco", nullable = false)),
        @AttributeOverride(name = "idPessoa", column = @Column(name = "IdPessoa", nullable = false))})
    public EnderecopessoaId getId() {
        return this.id;
    }

    public void setId(EnderecopessoaId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdEndereco", nullable = false, insertable = false, updatable = false)
    public Endereco getEndereco() {
        return this.endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdPessoa", nullable = false, insertable = false, updatable = false)
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Column(name = "Numero", nullable = false)
    public short getNumero() {
        return this.numero;
    }

    public void setNumero(short numero) {
        this.numero = numero;
    }

    @Column(name = "Complemeto", length = 60)
    public String getComplemeto() {
        return this.complemeto;
    }

    public void setComplemeto(String complemeto) {
        this.complemeto = complemeto;
    }

    @Column(name = "Observacao")
    public String getObservacao() {
        return this.observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

}
