package business;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ItemordemservicoId implements java.io.Serializable {

    private int idProduto;
    private int idOrdemServico;

    public ItemordemservicoId() {
    }

    public ItemordemservicoId(int idProduto, int idOrdemServico) {
        this.idProduto = idProduto;
        this.idOrdemServico = idOrdemServico;
    }

    @Column(name = "IdProduto", nullable = false)
    public int getIdProduto() {
        return this.idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    @Column(name = "IdOrdemServico", nullable = false)
    public int getIdOrdemServico() {
        return this.idOrdemServico;
    }

    public void setIdOrdemServico(int idOrdemServico) {
        this.idOrdemServico = idOrdemServico;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof ItemordemservicoId)) {
            return false;
        }
        ItemordemservicoId castOther = (ItemordemservicoId) other;

        return (this.getIdProduto() == castOther.getIdProduto())
                && (this.getIdOrdemServico() == castOther.getIdOrdemServico());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getIdProduto();
        result = 37 * result + this.getIdOrdemServico();
        return result;
    }

}
