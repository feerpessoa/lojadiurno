package business;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "itemordemservico", catalog = "lojadiurno"
)
public class Itemordemservico implements java.io.Serializable {

    private ItemordemservicoId id;
    private Ordemservico ordemservico;
    private Produto produto;
    private BigDecimal quantidade;
    private BigDecimal valorItem;

    public Itemordemservico() {
    }

    public Itemordemservico(ItemordemservicoId id, Ordemservico ordemservico, Produto produto, BigDecimal quantidade, BigDecimal valorItem) {
        this.id = id;
        this.ordemservico = ordemservico;
        this.produto = produto;
        this.quantidade = quantidade;
        this.valorItem = valorItem;
    }

    @EmbeddedId

    @AttributeOverrides({
        @AttributeOverride(name = "idProduto", column = @Column(name = "IdProduto", nullable = false)),
        @AttributeOverride(name = "idOrdemServico", column = @Column(name = "IdOrdemServico", nullable = false))})
    public ItemordemservicoId getId() {
        return this.id;
    }

    public void setId(ItemordemservicoId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdOrdemServico", nullable = false, insertable = false, updatable = false)
    public Ordemservico getOrdemservico() {
        return this.ordemservico;
    }

    public void setOrdemservico(Ordemservico ordemservico) {
        this.ordemservico = ordemservico;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false, insertable = false, updatable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Column(name = "Quantidade", nullable = false, precision = 10)
    public BigDecimal getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    @Column(name = "ValorItem", nullable = false, precision = 10)
    public BigDecimal getValorItem() {
        return this.valorItem;
    }

    public void setValorItem(BigDecimal valorItem) {
        this.valorItem = valorItem;
    }

}
