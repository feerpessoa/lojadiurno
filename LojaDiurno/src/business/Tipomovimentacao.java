package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipomovimentacao", catalog = "lojadiurno"
)
public class Tipomovimentacao implements java.io.Serializable {

    private Short idTipoMovimentacao;
    private String descricao;
    private Set<Movimentacao> movimentacaos = new HashSet<Movimentacao>(0);

    public Tipomovimentacao() {
    }

    public Tipomovimentacao(String descricao) {
        this.descricao = descricao;
    }

    public Tipomovimentacao(String descricao, Set<Movimentacao> movimentacaos) {
        this.descricao = descricao;
        this.movimentacaos = movimentacaos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdTipoMovimentacao", unique = true, nullable = false)
    public Short getIdTipoMovimentacao() {
        return this.idTipoMovimentacao;
    }

    public void setIdTipoMovimentacao(Short idTipoMovimentacao) {
        this.idTipoMovimentacao = idTipoMovimentacao;
    }

    @Column(name = "Descricao", nullable = false, length = 50)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tipomovimentacao")
    public Set<Movimentacao> getMovimentacaos() {
        return this.movimentacaos;
    }

    public void setMovimentacaos(Set<Movimentacao> movimentacaos) {
        this.movimentacaos = movimentacaos;
    }

}
