package business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "compra", catalog = "lojadiurno"
)
public class Compra implements java.io.Serializable {

    private Integer idCompra;
    private Fornecedor fornecedor;
    private String numeroNotaFiscal;
    private Date dataNotaFiscal;
    private BigDecimal valorNotaFiscal;
    private BigDecimal valorTotal;
    private Set<Itemcompra> itemcompras = new HashSet<Itemcompra>(0);

    public Compra() {
    }

    public Compra(Fornecedor fornecedor, String numeroNotaFiscal, Date dataNotaFiscal, BigDecimal valorNotaFiscal, BigDecimal valorTotal) {
        this.fornecedor = fornecedor;
        this.numeroNotaFiscal = numeroNotaFiscal;
        this.dataNotaFiscal = dataNotaFiscal;
        this.valorNotaFiscal = valorNotaFiscal;
        this.valorTotal = valorTotal;
    }

    public Compra(Fornecedor fornecedor, String numeroNotaFiscal, Date dataNotaFiscal, BigDecimal valorNotaFiscal, BigDecimal valorTotal, Set<Itemcompra> itemcompras) {
        this.fornecedor = fornecedor;
        this.numeroNotaFiscal = numeroNotaFiscal;
        this.dataNotaFiscal = dataNotaFiscal;
        this.valorNotaFiscal = valorNotaFiscal;
        this.valorTotal = valorTotal;
        this.itemcompras = itemcompras;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdCompra", unique = true, nullable = false)
    public Integer getIdCompra() {
        return this.idCompra;
    }

    public void setIdCompra(Integer idCompra) {
        this.idCompra = idCompra;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdFornecedor", nullable = false)
    public Fornecedor getFornecedor() {
        return this.fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    @Column(name = "NumeroNotaFiscal", nullable = false, length = 200)
    public String getNumeroNotaFiscal() {
        return this.numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DataNotaFiscal", nullable = false, length = 10)
    public Date getDataNotaFiscal() {
        return this.dataNotaFiscal;
    }

    public void setDataNotaFiscal(Date dataNotaFiscal) {
        this.dataNotaFiscal = dataNotaFiscal;
    }

    @Column(name = "ValorNotaFiscal", nullable = false, precision = 10)
    public BigDecimal getValorNotaFiscal() {
        return this.valorNotaFiscal;
    }

    public void setValorNotaFiscal(BigDecimal valorNotaFiscal) {
        this.valorNotaFiscal = valorNotaFiscal;
    }

    @Column(name = "ValorTotal", nullable = false, precision = 10)
    public BigDecimal getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "compra")
    public Set<Itemcompra> getItemcompras() {
        return this.itemcompras;
    }

    public void setItemcompras(Set<Itemcompra> itemcompras) {
        this.itemcompras = itemcompras;
    }

}
