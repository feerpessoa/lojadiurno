package business;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "itemcompra", catalog = "lojadiurno"
)
public class Itemcompra implements java.io.Serializable {

    private ItemcompraId id;
    private Compra compra;
    private Produto produto;
    private BigDecimal quantidade;
    private BigDecimal valorItem;

    public Itemcompra() {
    }

    public Itemcompra(ItemcompraId id, Compra compra, Produto produto, BigDecimal quantidade, BigDecimal valorItem) {
        this.id = id;
        this.compra = compra;
        this.produto = produto;
        this.quantidade = quantidade;
        this.valorItem = valorItem;
    }

    @EmbeddedId

    @AttributeOverrides({
        @AttributeOverride(name = "idCompra", column = @Column(name = "IdCompra", nullable = false)),
        @AttributeOverride(name = "idProduto", column = @Column(name = "IdProduto", nullable = false))})
    public ItemcompraId getId() {
        return this.id;
    }

    public void setId(ItemcompraId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdCompra", nullable = false, insertable = false, updatable = false)
    public Compra getCompra() {
        return this.compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false, insertable = false, updatable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Column(name = "Quantidade", nullable = false, precision = 10)
    public BigDecimal getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    @Column(name = "ValorItem", nullable = false, precision = 10)
    public BigDecimal getValorItem() {
        return this.valorItem;
    }

    public void setValorItem(BigDecimal valorItem) {
        this.valorItem = valorItem;
    }

}
