package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "fornecedor", catalog = "lojadiurno"
)
public class Fornecedor implements java.io.Serializable {

    private int idFornecedor;
    private Pessoajuridica pessoajuridica;
    private String site;
    private Set<Compra> compras = new HashSet<Compra>(0);

    public Fornecedor() {
    }

    public Fornecedor(Pessoajuridica pessoajuridica) {
        this.pessoajuridica = pessoajuridica;
    }

    public Fornecedor(Pessoajuridica pessoajuridica, String site, Set<Compra> compras) {
        this.pessoajuridica = pessoajuridica;
        this.site = site;
        this.compras = compras;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "pessoajuridica"))
    @Id
    @GeneratedValue(generator = "generator")

    @Column(name = "IdFornecedor", unique = true, nullable = false)
    public int getIdFornecedor() {
        return this.idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public Pessoajuridica getPessoajuridica() {
        return this.pessoajuridica;
    }

    public void setPessoajuridica(Pessoajuridica pessoajuridica) {
        this.pessoajuridica = pessoajuridica;
    }

    @Column(name = "Site")
    public String getSite() {
        return this.site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fornecedor")
    public Set<Compra> getCompras() {
        return this.compras;
    }

    public void setCompras(Set<Compra> compras) {
        this.compras = compras;
    }

}
