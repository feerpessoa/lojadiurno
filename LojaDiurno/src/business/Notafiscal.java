package business;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "notafiscal", catalog = "lojadiurno"
)
public class Notafiscal implements java.io.Serializable {

    private Integer idNotaFiscal;
    private Ordemservico ordemservico;
    private Venda venda;
    private Date dataEmissao;

    public Notafiscal() {
    }

    public Notafiscal(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Notafiscal(Ordemservico ordemservico, Venda venda, Date dataEmissao) {
        this.ordemservico = ordemservico;
        this.venda = venda;
        this.dataEmissao = dataEmissao;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdNotaFiscal", unique = true, nullable = false)
    public Integer getIdNotaFiscal() {
        return this.idNotaFiscal;
    }

    public void setIdNotaFiscal(Integer idNotaFiscal) {
        this.idNotaFiscal = idNotaFiscal;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdOrdemServico")
    public Ordemservico getOrdemservico() {
        return this.ordemservico;
    }

    public void setOrdemservico(Ordemservico ordemservico) {
        this.ordemservico = ordemservico;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdVenda")
    public Venda getVenda() {
        return this.venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DataEmissao", nullable = false, length = 19)
    public Date getDataEmissao() {
        return this.dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

}
