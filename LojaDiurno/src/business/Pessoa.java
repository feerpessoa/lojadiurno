package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pessoa", catalog = "lojadiurno"
)
public class Pessoa implements java.io.Serializable {

    private Integer idPessoa;
    private String nome;
    private String email;
    private String observacao;
    private Cliente cliente;
    private Set<Telefone> telefones = new HashSet<Telefone>(0);
    private Set<Enderecopessoa> enderecopessoas = new HashSet<Enderecopessoa>(0);
    private Vendedor vendedor;
    private Pessoafisica pessoafisica;
    private Pessoajuridica pessoajuridica;

    public Pessoa() {
    }

    public Pessoa(String nome) {
        this.nome = nome;
    }

    public Pessoa(String nome, String email, String observacao, Cliente cliente, Set telefones, Set<Enderecopessoa> enderecopessoas, Vendedor vendedor, Pessoafisica pessoafisica, Pessoajuridica pessoajuridica) {
        this.nome = nome;
        this.email = email;
        this.observacao = observacao;
        this.cliente = cliente;
        this.telefones = telefones;
        this.enderecopessoas = enderecopessoas;
        this.vendedor = vendedor;
        this.pessoafisica = pessoafisica;
        this.pessoajuridica = pessoajuridica;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdPessoa", unique = true, nullable = false)
    public Integer getIdPessoa() {
        return this.idPessoa;
    }

    public void setIdPessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }

    @Column(name = "Nome", nullable = false, length = 200)
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name = "Email")
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "Observacao", length = 65535)
    public String getObservacao() {
        return this.observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "pessoa")
    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pessoa")
    public Set<Telefone> getTelefones() {
        return this.telefones;
    }

    public void setTelefones(Set<Telefone> telefones) {
        this.telefones = telefones;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pessoa")
    public Set<Enderecopessoa> getEnderecopessoas() {
        return this.enderecopessoas;
    }

    public void setEnderecopessoas(Set<Enderecopessoa> enderecopessoas) {
        this.enderecopessoas = enderecopessoas;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "pessoa")
    public Vendedor getVendedor() {
        return this.vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "pessoa")
    public Pessoafisica getPessoafisica() {
        return this.pessoafisica;
    }

    public void setPessoafisica(Pessoafisica pessoafisica) {
        this.pessoafisica = pessoafisica;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "pessoa", cascade = CascadeType.ALL)
    public Pessoajuridica getPessoajuridica() {
        return this.pessoajuridica;
    }

    public void setPessoajuridica(Pessoajuridica pessoajuridica) {
        this.pessoajuridica = pessoajuridica;
    }

}
