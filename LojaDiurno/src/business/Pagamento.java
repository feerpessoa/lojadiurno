package business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pagamento", catalog = "lojadiurno"
)
public class Pagamento implements java.io.Serializable {

    private Short idPagamento;
    private Tipopagamento tipopagamento;
    private Integer numeroTransacao;
    private Integer quantidadeParcela;
    private Integer valorParcela;
    private BigDecimal valor;
    private String nomePortador;
    private Date dataRetirada;
    private String telefone;
    private Boolean edebito;
    private Set<Ordemservico> ordemservicos = new HashSet<Ordemservico>(0);
    private Set<Venda> vendas = new HashSet<Venda>(0);

    public Pagamento() {
    }

    public Pagamento(Tipopagamento tipopagamento, BigDecimal valor, String telefone) {
        this.tipopagamento = tipopagamento;
        this.valor = valor;
        this.telefone = telefone;
    }

    public Pagamento(Tipopagamento tipopagamento, Integer numeroTransacao, Integer quantidadeParcela, Integer valorParcela, BigDecimal valor, String nomePortador, Date dataRetirada, String telefone, Boolean edebito, Set<Ordemservico> ordemservicos, Set<Venda> vendas) {
        this.tipopagamento = tipopagamento;
        this.numeroTransacao = numeroTransacao;
        this.quantidadeParcela = quantidadeParcela;
        this.valorParcela = valorParcela;
        this.valor = valor;
        this.nomePortador = nomePortador;
        this.dataRetirada = dataRetirada;
        this.telefone = telefone;
        this.edebito = edebito;
        this.ordemservicos = ordemservicos;
        this.vendas = vendas;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdPagamento", unique = true, nullable = false)
    public Short getIdPagamento() {
        return this.idPagamento;
    }

    public void setIdPagamento(Short idPagamento) {
        this.idPagamento = idPagamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdTipoPagamento", nullable = false)
    public Tipopagamento getTipopagamento() {
        return this.tipopagamento;
    }

    public void setTipopagamento(Tipopagamento tipopagamento) {
        this.tipopagamento = tipopagamento;
    }

    @Column(name = "NumeroTransacao")
    public Integer getNumeroTransacao() {
        return this.numeroTransacao;
    }

    public void setNumeroTransacao(Integer numeroTransacao) {
        this.numeroTransacao = numeroTransacao;
    }

    @Column(name = "QuantidadeParcela")
    public Integer getQuantidadeParcela() {
        return this.quantidadeParcela;
    }

    public void setQuantidadeParcela(Integer quantidadeParcela) {
        this.quantidadeParcela = quantidadeParcela;
    }

    @Column(name = "ValorParcela")
    public Integer getValorParcela() {
        return this.valorParcela;
    }

    public void setValorParcela(Integer valorParcela) {
        this.valorParcela = valorParcela;
    }

    @Column(name = "Valor", nullable = false, precision = 10)
    public BigDecimal getValor() {
        return this.valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Column(name = "NomePortador", length = 100)
    public String getNomePortador() {
        return this.nomePortador;
    }

    public void setNomePortador(String nomePortador) {
        this.nomePortador = nomePortador;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DataRetirada", length = 19)
    public Date getDataRetirada() {
        return this.dataRetirada;
    }

    public void setDataRetirada(Date dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    @Column(name = "Telefone", nullable = false, length = 14)
    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Column(name = "EDebito")
    public Boolean getEdebito() {
        return this.edebito;
    }

    public void setEdebito(Boolean edebito) {
        this.edebito = edebito;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pagamento")
    public Set<Ordemservico> getOrdemservicos() {
        return this.ordemservicos;
    }

    public void setOrdemservicos(Set<Ordemservico> ordemservicos) {
        this.ordemservicos = ordemservicos;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pagamento")
    public Set<Venda> getVendas() {
        return this.vendas;
    }

    public void setVendas(Set<Venda> vendas) {
        this.vendas = vendas;
    }

}
