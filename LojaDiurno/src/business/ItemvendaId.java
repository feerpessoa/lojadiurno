package business;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ItemvendaId implements java.io.Serializable {

    private int idVenda;
    private int idProduto;

    public ItemvendaId() {
    }

    public ItemvendaId(int idVenda, int idProduto) {
        this.idVenda = idVenda;
        this.idProduto = idProduto;
    }

    @Column(name = "IdVenda", nullable = false)
    public int getIdVenda() {
        return this.idVenda;
    }

    public void setIdVenda(int idVenda) {
        this.idVenda = idVenda;
    }

    @Column(name = "IdProduto", nullable = false)
    public int getIdProduto() {
        return this.idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof ItemvendaId)) {
            return false;
        }
        ItemvendaId castOther = (ItemvendaId) other;

        return (this.getIdVenda() == castOther.getIdVenda())
                && (this.getIdProduto() == castOther.getIdProduto());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getIdVenda();
        result = 37 * result + this.getIdProduto();
        return result;
    }

}
