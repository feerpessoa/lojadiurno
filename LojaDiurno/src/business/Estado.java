package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "estado", catalog = "lojadiurno"
)
public class Estado implements java.io.Serializable {

    private String uf;
    private String estado;
    private int codIbge;
    private Set<Cidade> cidades = new HashSet<Cidade>(0);

    public Estado() {
    }

    public Estado(String uf, String estado, int codIbge) {
        this.uf = uf;
        this.estado = estado;
        this.codIbge = codIbge;
    }

    public Estado(String uf, String estado, int codIbge, Set<Cidade> cidades) {
        this.uf = uf;
        this.estado = estado;
        this.codIbge = codIbge;
        this.cidades = cidades;
    }

    @Id

    @Column(name = "UF", unique = true, nullable = false, length = 2)
    public String getUf() {
        return this.uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    @Column(name = "Estado", nullable = false, length = 150)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "CodIbge", nullable = false)
    public int getCodIbge() {
        return this.codIbge;
    }

    public void setCodIbge(int codIbge) {
        this.codIbge = codIbge;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "estado")
    public Set<Cidade> getCidades() {
        return this.cidades;
    }

    public void setCidades(Set<Cidade> cidades) {
        this.cidades = cidades;
    }

}
