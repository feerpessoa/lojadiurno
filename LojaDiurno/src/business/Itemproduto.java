package business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "itemproduto", catalog = "lojadiurno"
)
public class Itemproduto implements java.io.Serializable {

    private Integer idItemProduto;
    private Produto produto;
    private String descricao;
    private String valor;

    public Itemproduto() {
    }

    public Itemproduto(Produto produto, String descricao, String valor) {
        this.produto = produto;
        this.descricao = descricao;
        this.valor = valor;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdItemProduto", unique = true, nullable = false)
    public Integer getIdItemProduto() {
        return this.idItemProduto;
    }

    public void setIdItemProduto(Integer idItemProduto) {
        this.idItemProduto = idItemProduto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Column(name = "Descricao", nullable = false, length = 80)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "Valor", nullable = false, length = 65535)
    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
