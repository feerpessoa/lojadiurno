package business;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "pessoafisica", catalog = "lojadiurno", uniqueConstraints = @UniqueConstraint(columnNames = "CPF")
)
public class Pessoafisica implements java.io.Serializable {

    private Integer idPessoaFisica;
    private Pessoa pessoa;
    private Date dataNascimento;
    private char sexo;
    private String rg;
    private String cpf;

    public Pessoafisica() {
    }

    public Pessoafisica(Pessoa pessoa, Date dataNascimento, char sexo, String cpf) {
        this.pessoa = pessoa;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.cpf = cpf;
    }

    public Pessoafisica(Pessoa pessoa, Date dataNascimento, char sexo, String rg, String cpf) {
        this.pessoa = pessoa;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.rg = rg;
        this.cpf = cpf;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "pessoa"))
    @Id
    @GeneratedValue(generator = "generator")

    @Column(name = "IdPessoaFisica", unique = true, nullable = false)
    public Integer getIdPessoaFisica() {
        return this.idPessoaFisica;
    }

    public void setIdPessoaFisica(Integer idPessoaFisica) {
        this.idPessoaFisica = idPessoaFisica;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DataNascimento", nullable = false, length = 10)
    public Date getDataNascimento() {
        return this.dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Column(name = "Sexo", nullable = false, length = 1)
    public char getSexo() {
        return this.sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    @Column(name = "RG", length = 20)
    public String getRg() {
        return this.rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    @Column(name = "CPF", unique = true, nullable = false, length = 14)
    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
