package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "cliente", catalog = "lojadiurno"
)
public class Cliente implements java.io.Serializable {

    private Integer idCliente;
    private Pessoa pessoa;
    private Set<Venda> vendas = new HashSet<Venda>(0);
    private Set<Ordemservico> ordemservicos = new HashSet<Ordemservico>(0);

    public Cliente() {
    }

    public Cliente(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Cliente(Pessoa pessoa, Set<Venda> vendas, Set<Ordemservico> ordemservicos) {
        this.pessoa = pessoa;
        this.vendas = vendas;
        this.ordemservicos = ordemservicos;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "pessoa"))
    @Id
    @GeneratedValue(generator = "generator")

    @Column(name = "IdCliente", unique = true, nullable = false)
    public Integer getIdCliente() {
        return this.idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    public Set<Venda> getVendas() {
        return this.vendas;
    }

    public void setVendas(Set<Venda> vendas) {
        this.vendas = vendas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    public Set<Ordemservico> getOrdemservicos() {
        return this.ordemservicos;
    }

    public void setOrdemservicos(Set<Ordemservico> ordemservicos) {
        this.ordemservicos = ordemservicos;
    }

}
