package business;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "vigenciapreco", catalog = "lojadiurno"
)
public class Vigenciapreco implements java.io.Serializable {

    private VigenciaprecoId id;
    private Produto produto;
    private Date dataFim;
    private BigDecimal valorProduto;

    public Vigenciapreco() {
    }

    public Vigenciapreco(VigenciaprecoId id, Produto produto, BigDecimal valorProduto) {
        this.id = id;
        this.produto = produto;
        this.valorProduto = valorProduto;
    }

    public Vigenciapreco(VigenciaprecoId id, Produto produto, Date dataFim, BigDecimal valorProduto) {
        this.id = id;
        this.produto = produto;
        this.dataFim = dataFim;
        this.valorProduto = valorProduto;
    }

    @EmbeddedId

    @AttributeOverrides({
        @AttributeOverride(name = "idProduto", column = @Column(name = "IdProduto", nullable = false)),
        @AttributeOverride(name = "dataInicio", column = @Column(name = "DataInicio", nullable = false, length = 19))})
    public VigenciaprecoId getId() {
        return this.id;
    }

    public void setId(VigenciaprecoId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdProduto", nullable = false, insertable = false, updatable = false)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DataFim", length = 19)
    public Date getDataFim() {
        return this.dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    @Column(name = "ValorProduto", nullable = false, precision = 10)
    public BigDecimal getValorProduto() {
        return this.valorProduto;
    }

    public void setValorProduto(BigDecimal valorProduto) {
        this.valorProduto = valorProduto;
    }

}
