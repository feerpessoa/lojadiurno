package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipopagamento", catalog = "lojadiurno"
)
public class Tipopagamento implements java.io.Serializable {

    private Short idTipoPagamento;
    private String descricao;
    private Set<Pagamento> pagamentos = new HashSet<Pagamento>(0);

    public Tipopagamento() {
    }

    public Tipopagamento(String descricao) {
        this.descricao = descricao;
    }

    public Tipopagamento(String descricao, Set<Pagamento> pagamentos) {
        this.descricao = descricao;
        this.pagamentos = pagamentos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdTipoPagamento", unique = true, nullable = false)
    public Short getIdTipoPagamento() {
        return this.idTipoPagamento;
    }

    public void setIdTipoPagamento(Short idTipoPagamento) {
        this.idTipoPagamento = idTipoPagamento;
    }

    @Column(name = "Descricao", nullable = false, length = 80)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tipopagamento")
    public Set<Pagamento> getPagamentos() {
        return this.pagamentos;
    }

    public void setPagamentos(Set<Pagamento> pagamentos) {
        this.pagamentos = pagamentos;
    }

}
