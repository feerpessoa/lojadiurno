package business;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipotelefone", catalog = "lojadiurno"
)
public class Tipotelefone implements java.io.Serializable {

    private Short idTipoTelefone;
    private String descricao;
    private Set<Telefone> telefones = new HashSet<Telefone>(0);

    public Tipotelefone() {
    }

    public Tipotelefone(String descricao) {
        this.descricao = descricao;
    }

    public Tipotelefone(String descricao, Set<Telefone> telefones) {
        this.descricao = descricao;
        this.telefones = telefones;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "IdTipoTelefone", unique = true, nullable = false)
    public Short getIdTipoTelefone() {
        return this.idTipoTelefone;
    }

    public void setIdTipoTelefone(Short idTipoTelefone) {
        this.idTipoTelefone = idTipoTelefone;
    }

    @Column(name = "Descricao", nullable = false, length = 100)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tipotelefone")
    public Set<Telefone> getTelefones() {
        return this.telefones;
    }

    public void setTelefones(Set<Telefone> telefones) {
        this.telefones = telefones;
    }
    
    @Override
    public String toString() {
        return descricao;
    }

}
