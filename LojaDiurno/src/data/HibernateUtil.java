package data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil 
{
	private static SessionFactory sessionFactory;
	
	//ao carregar o projeto o m�todo � executado automaticamente uma �nica vez
	//respons�vel por conectar ao banco de dados e carregar o arquivo hibernate.cfg.xml de configura��o
	static
	{
		try
		{
			sessionFactory =
					new Configuration().configure().buildSessionFactory();
		}
		
		catch(Throwable ex)
		{
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static Session getSession()
	{  
        return sessionFactory.openSession();
    }  
	
	public static SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
}